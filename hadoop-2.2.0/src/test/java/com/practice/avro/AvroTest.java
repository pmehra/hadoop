package com.practice.avro;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Iterator;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.file.FileReader;
import org.apache.avro.file.SeekableInput;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.mapred.FsInput;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import com.practice.hadoop.CommonOperations;

public class AvroTest {

	@Test
	public void avroMRJobTest() throws Exception {
		Configuration conf = CommonOperations.getLocalConfiguration();
		String inputFilePath = CommonOperations
				.getClassPathFileAbsPath("airline/airlines.txt");
		String outputDirPath = CommonOperations
				.getFindableTempPath("avroMRJob");

		AirlineDataAvroFormatterMRJob driver = new AirlineDataAvroFormatterMRJob();
		driver.setConf(conf);

		int exitCode = driver
				.run(new String[] { inputFilePath, outputDirPath });
		Assert.assertEquals(0, exitCode);

		FileSystem fs = FileSystem.get(URI.create(outputDirPath), conf);
		FileStatus[] listStatus = fs.listStatus(new Path(outputDirPath));
		Path[] paths = FileUtil.stat2Paths(listStatus);
		try {
			for (Path path : paths) {

				Assert.assertThat(path.getName(), Matchers.anyOf(
						startsWith("_SUCCESS"), startsWith("part")));
			}

		} finally {
			// HDFSOperations.deleteFile(conf, outputDirPath);
		}
	}

	@Test
	public void writingAvroToFile() throws Exception {
		StringPair sp = new StringPair("mL", "mR");

		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		DatumWriter<StringPair> writer = new SpecificDatumWriter<StringPair>();

		DataFileWriter<StringPair> fileWriter = new DataFileWriter<>(writer);
		fileWriter.create(sp.getSchema(), bout);
		fileWriter.append(sp);
		fileWriter.flush();
		fileWriter.close();
		bout.flush();

		File tempFile = File.createTempFile("pairAvroData", ".avro");
		tempFile.deleteOnExit();
		OutputStream out = new FileOutputStream(tempFile);
		bout.writeTo(out);
		bout.close();

		DatumReader<GenericRecord> reader = new GenericDatumReader<>();
		DataFileReader<GenericRecord> fileReader = new DataFileReader<>(
				tempFile, reader);

		assertThat(sp.getSchema(), is(fileReader.getSchema()));
		Iterator<GenericRecord> it = fileReader.iterator();
		while (it.hasNext()) {
			GenericRecord gr = it.next();
			assertThat(gr.get("L").toString(), is("mL"));
			assertThat(gr.get("R").toString(), is("mR"));

		}

		fileReader.close();

	}

	@Test
	public void writingAndReadingToHDFS() throws Exception {
		String path = CommonOperations
				.getClassPathFileAbsPath("avrofiles/stringpairData.avro");
		SeekableInput fsInput = new FsInput(new Path(path),
				CommonOperations.getLocalConfiguration());

		DatumReader<GenericRecord> reader = new GenericDatumReader<>();
		FileReader<GenericRecord> fileReader = DataFileReader.openReader(
				fsInput, reader);

		for (GenericRecord gr : fileReader) {
			assertThat(gr.get("L").toString(), is("isLeft"));
			assertThat(gr.get("R").toString(), is("isRight"));
		}

	}

	@Test
	public void extraFieldReadSchemaDifferent() throws Exception {
		String path = CommonOperations
				.getClassPathFileAbsPath("avrofiles/stringpairData.avro");
		File f = new File(path);

		Schema.Parser parser = new Schema.Parser();
		InputStream resourceAsStream = ClassLoader.getSystemClassLoader()
				.getResourceAsStream("avrofiles/stringpairWithExtraField.avsc");
		Schema readerSchema = parser.parse(resourceAsStream);

		DatumReader<GenericRecord> dataReader = new GenericDatumReader<>(null,
				readerSchema);
		DataFileReader<GenericRecord> fileReader = new DataFileReader<>(f,
				dataReader);

		for (GenericRecord gr : fileReader) {
			assertThat(gr.get("L").toString(), is("isLeft"));
			assertThat(gr.get("R").toString(), is("isRight"));
			assertThat(gr.get("desc").toString(), is("default desc"));
		}
		fileReader.close();
	}

	@Test
	public void withoutFieldSchemaRead() throws Exception {
		String path = CommonOperations
				.getClassPathFileAbsPath("avrofiles/stringpairData.avro");
		File f = new File(path);

		Schema.Parser parser = new Schema.Parser();
		InputStream resourceAsStream = ClassLoader.getSystemClassLoader()
				.getResourceAsStream("avrofiles/stringpairWithoutField.avsc");
		Schema readerSchema = parser.parse(resourceAsStream);

		DatumReader<GenericRecord> dataReader = new GenericDatumReader<>(null,
				readerSchema);
		DataFileReader<GenericRecord> fileReader = new DataFileReader<>(f,
				dataReader);

		for (GenericRecord gr : fileReader) {
			assertThat(gr.get("L").toString(), is("isLeft"));
			assertThat(gr.get("R"), nullValue());
			assertThat(gr.get("desc"), nullValue());
		}
		fileReader.close();
	}

	@Test
	public void readSchemaWithGeneratedFile() throws Exception {
		StringPair sp = new StringPair();
		sp.setL("isLeft");
		sp.setR("isR");

		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		DatumWriter<StringPair> writer = new SpecificDatumWriter<>(
				StringPair.class);
		Encoder encoder = EncoderFactory.get().binaryEncoder(bout, null);
		writer.write(sp, encoder);
		encoder.flush();
		bout.close();

		DatumReader<StringPair> reader = new SpecificDatumReader<>(
				StringPair.class);
		Decoder decoder = DecoderFactory.get().binaryDecoder(
				bout.toByteArray(), null);
		sp = reader.read(null, decoder);

		assertThat(sp.getL(), is("isLeft"));
		assertThat(sp.getR(), is("isR"));
	}

	@Test
	public void readSchema() throws Exception {
		Schema.Parser parser = new Schema.Parser();
		InputStream resourceAsStream = ClassLoader.getSystemClassLoader()
				.getResourceAsStream("avrofiles/stringpair.avsc");
		Schema schema = parser.parse(resourceAsStream);

		GenericRecord gr = new GenericData.Record(schema);
		gr.put("L", "isLeft");
		gr.put("R", "isRight");

		ByteArrayOutputStream out = new ByteArrayOutputStream();

		DatumWriter<GenericRecord> writer = new GenericDatumWriter<>(schema);
		Encoder encoder = EncoderFactory.get().binaryEncoder(out, null);
		writer.write(gr, encoder);
		encoder.flush();
		out.close();

		DatumReader<GenericRecord> reader = new GenericDatumReader<>(schema);
		Decoder decoder = DecoderFactory.get().binaryDecoder(out.toByteArray(),
				null);
		GenericRecord readGr = reader.read(null, decoder);

		Assert.assertEquals("isLeft", readGr.get("L").toString());
		Assert.assertEquals("isRight", readGr.get("R").toString());

	}

}
