package com.practice.codec;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;

import org.apache.commons.compress.archivers.dump.UnsupportedCompressionAlgorithmException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.io.compress.CompressionOutputStream;
import org.apache.hadoop.io.compress.GzipCodec;
import org.junit.Test;

public class CodecTest {

	@Test
	public void codecTest() throws Exception {
		GzipCodec gzipCodec = new GzipCodec();

		File tempFile = File.createTempFile("file", ".gz");
		tempFile.deleteOnExit();

		FileOutputStream fout = new FileOutputStream(tempFile);
		CompressionOutputStream cout = gzipCodec.createOutputStream(fout);
		cout.write("Hello".getBytes());
		cout.finish();

		
		String tempFilePath = tempFile.getAbsolutePath();
		System.out.println(tempFilePath);
		
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(URI.create(tempFilePath),conf);
		Path path = new Path(tempFilePath);
		
		CompressionCodecFactory factory = new CompressionCodecFactory(conf);
		CompressionCodec codec = factory.getCodec(path);
		if(codec == null){
			throw new UnsupportedCompressionAlgorithmException();
		}
		
		String outputPath = CompressionCodecFactory.removeSuffix(tempFilePath, codec.getDefaultExtension());
		System.out.println(outputPath);
		InputStream in = codec.createInputStream(fs.open(path));
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		IOUtils.copyBytes(in, bout, conf);
		in.close();
		bout.close();
		
		System.out.println(bout.toString());
		
	}
}
