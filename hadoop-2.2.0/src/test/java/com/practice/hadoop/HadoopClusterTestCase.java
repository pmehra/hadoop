package com.practice.hadoop;

import org.apache.hadoop.mapred.ClusterMapReduceTestCase;

/**
 * This class extends {@link ClusterMapReduceTestCase} which automatically starts and stop cluster.
 * You can have following cluster
 * -HDFS
 * -YARN
 * 
 * @author pmehra
 *
 */
public class HadoopClusterTestCase extends ClusterMapReduceTestCase{

}
