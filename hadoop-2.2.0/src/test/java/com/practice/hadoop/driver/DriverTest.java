package com.practice.hadoop.driver;

import static org.hamcrest.CoreMatchers.startsWith;

import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import com.practice.hadoop.CommonOperations;

public class DriverTest {

	/**
	 * This test will not work in local but will work in hadoop pseudo distributed mode
	 * In local it cannot deduce the codec and uncompressed it but it does it in pseudo mode.
	 * 
	 * @throws Exception
	 */
	@Test
	public void readrCompressedDataMRJob() throws Exception {

		Configuration conf = CommonOperations.getLocalConfiguration();
		String inputFilePath = CommonOperations
				.getClassPathFileAbsPath("airline/airlinesCompress.gzip");
		String outputDirPath = CommonOperations
				.getFindableTempPath("readCompressedDataMRJob");

		ReadCompressedAggregateDriver driver = new ReadCompressedAggregateDriver();
		driver.setConf(conf);

		int exitCode = driver
				.run(new String[] { inputFilePath, outputDirPath });
		Assert.assertEquals(0, exitCode);

		FileSystem fs = FileSystem.get(URI.create(outputDirPath), conf);
		FileStatus[] listStatus = fs.listStatus(new Path(outputDirPath));
		Path[] paths = FileUtil.stat2Paths(listStatus);
		try {
			for (Path path : paths) {

				Assert.assertThat(path.getName(), Matchers.anyOf(
						startsWith("_SUCCESS"), startsWith("part")));
			}

		} finally {
			// HDFSOperations.deleteFile(conf, outputDirPath);
		}

	}

}
