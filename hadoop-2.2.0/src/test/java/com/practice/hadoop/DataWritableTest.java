package com.practice.hadoop;

import static org.hamcrest.Matchers.is;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparator;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class DataWritableTest {

	@Test
	public void serializeTest() throws Exception {
		Writable writable = new IntWritable(2);
		byte[] byteArray = serialize(writable);
		Assert.assertThat(byteArray.length, Matchers.is(4));
	}

	@Test
	public void deserializeTest() throws Exception {
		IntWritable writable = new IntWritable(2);
		byte[] serializedBytes = serialize(writable);

		writable = new IntWritable();
		deserialize(writable, serializedBytes);

		Assert.assertThat(writable.get(), is(2));
	}

	@Test
	public void rawComparatorTest() throws IOException {
		WritableComparator writableComparator = WritableComparator
				.get(IntWritable.class);

		IntWritable writableA = new IntWritable(2);
		IntWritable writableB = new IntWritable(2);

		Assert.assertThat(writableA, is(writableB));
		Assert.assertThat(writableComparator.compare(writableA, writableB),
				is(0));
		byte[] a = serialize(writableA);
		byte[] b = serialize(writableB);
		Assert.assertThat(
				writableComparator.compare(a, 0, a.length, b, 0, b.length),
				Matchers.is(0));
	}
	
	public void txtArrayWritableTest(){
		ArrayWritable arr=new TextArrayWritable();
	}

	private static class TextArrayWritable extends ArrayWritable {

		public TextArrayWritable() {
			super(Text.class);
		}
	}

	private void deserialize(Writable writable, byte[] serializedBytes)
			throws IOException {
		ByteArrayInputStream bin = new ByteArrayInputStream(serializedBytes);
		DataInputStream din = new DataInputStream(bin);
		writable.readFields(din);
		din.close();
	}

	private byte[] serialize(Writable writable) throws IOException {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		DataOutputStream dout = new DataOutputStream(bout);
		writable.write(dout);
		dout.close();
		return bout.toByteArray();
	}

}
