package com.practice.hadoop;

import org.junit.Test;

public class CommonOperationTest {

	@Test
	public void createCompressedFile() {
		String inputFile = CommonOperations.getClassPathFileAbsPath("airlines/airlines.txt");
		CommonOperations.compressFile(CodecType.GZIP, inputFile, CommonOperations.getLocalConfiguration());
	}
}
