package com.practice.hadoop;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class HDFSOperationsTest {

	private static final String ncdc_sample = "ncdc/sample.txt";

	/**
	 * To get absolute path of any file from classpath --> String inputPath =
	 * ClassLoader.getSystemClassLoader() .getResource(ncdc_sample).getPath();
	 */
	@Test
	public void copyClasspathFileTest() throws IOException {

		Configuration conf = new Configuration();
		conf.addResource("hadoop-localhost.xml");
		
		String inputHdfs = "/data/ncdc";
		String fileNameInHdfs = "sample.txt";

		HDFSOperations.copyFromLocal(conf, ncdc_sample, inputHdfs,
				fileNameInHdfs);

		Path outHdfs = new Path(inputHdfs + "/" + fileNameInHdfs);
		FileSystem fs = FileSystem.get(URI.create(outHdfs.toString()), conf);
		Assert.assertTrue("file transfer failed", fs.exists(outHdfs));
	}

	@Test
	@Ignore
	public void deleteFileTest() throws IOException {

		Configuration conf = new Configuration();
		conf.addResource("hadoop-localhost.xml");

		String location = "/data/ncdc/sample.txt";
		HDFSOperations.deleteFile(conf, location);

		FileSystem fs = FileSystem.get(URI.create(location), conf);
		Assert.assertFalse("file not deleted", fs.exists(new Path(location)));
	}
}
