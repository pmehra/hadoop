package com.practice.hadoop.airline;

import static org.hamcrest.CoreMatchers.startsWith;

import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import com.practice.hadoop.CommonOperations;
import com.practice.hadoop.HDFSOperations;

public class AirlineMRJobTest {

	@Test
	public void selectMRJobTest() throws Exception {

		Configuration conf = CommonOperations.getLocalConfiguration();
		String inputFilePath = CommonOperations
				.getClassPathFileAbsPath("airline/airline-sample.csv");
		String outputDirPath = CommonOperations
				.getFindableTempPath("airlineSelectMR");

		SelectMRJob driver = new SelectMRJob();
		driver.setConf(conf);

		int exitCode = driver
				.run(new String[] { inputFilePath, outputDirPath });
		Assert.assertEquals(0, exitCode);

		FileSystem fs = FileSystem.get(URI.create(outputDirPath), conf);
		FileStatus[] listStatus = fs.listStatus(new Path(outputDirPath));
		Path[] paths = FileUtil.stat2Paths(listStatus);
		try {
			for (Path path : paths) {

				Assert.assertThat(path.getName(), Matchers.anyOf(
						startsWith("_SUCCESS"), startsWith("part")));
			}

		} finally {
			HDFSOperations.deleteFile(conf, outputDirPath);
		}
	}

	@Test
	public void whereMRJobTest() throws Exception {
		Configuration conf = CommonOperations.getLocalConfiguration();
		conf.setLong(WhereMRJob.WhereMapper.DELAY_THRESHOLD_MIN, 10L);
		String inputFilePath = CommonOperations
				.getClassPathFileAbsPath("airline/airline-sample.csv");
		String outputDirPath = CommonOperations
				.getFindableTempPath("airlineWhereMR");

		WhereMRJob driver = new WhereMRJob();
		driver.setConf(conf);

		int exitCode = driver
				.run(new String[] { inputFilePath, outputDirPath });
		Assert.assertEquals(0, exitCode);

		FileSystem fs = FileSystem.get(URI.create(outputDirPath), conf);
		FileStatus[] listStatus = fs.listStatus(new Path(outputDirPath));
		Path[] paths = FileUtil.stat2Paths(listStatus);
		try {
			for (Path path : paths) {

				Assert.assertThat(path.getName(), Matchers.anyOf(
						startsWith("_SUCCESS"), startsWith("part")));
			}

		} finally {
			HDFSOperations.deleteFile(conf, outputDirPath);
		}

	}

	@Test
	public void aggregateMRJobTest() throws Exception {
		Configuration conf = CommonOperations.getLocalConfiguration();
		String inputFilePath = CommonOperations
				.getClassPathFileAbsPath("airline/airline-sample.csv");
		String outputDirPath = CommonOperations
				.getFindableTempPath("airlineAggregateMR");

		AggregateMRJob driver = new AggregateMRJob();
		driver.setConf(conf);

		int exitCode = driver
				.run(new String[] { inputFilePath, outputDirPath });
		Assert.assertEquals(0, exitCode);

		FileSystem fs = FileSystem.get(URI.create(outputDirPath), conf);
		FileStatus[] listStatus = fs.listStatus(new Path(outputDirPath));
		Path[] paths = FileUtil.stat2Paths(listStatus);
		try {
			for (Path path : paths) {

				Assert.assertThat(path.getName(), Matchers.anyOf(
						startsWith("_SUCCESS"), startsWith("part")));
			}

		} finally {
			HDFSOperations.deleteFile(conf, outputDirPath);
		}

	}
	
	@Test
	public void aggregateCobinerMRJobTest() throws Exception {
		Configuration conf = CommonOperations.getLocalConfiguration();
		String inputFilePath = CommonOperations
				.getClassPathFileAbsPath("airline/airline-sample.csv");
		String outputDirPath = CommonOperations
				.getFindableTempPath("airlineAggregateCombinerMR");

		AggregateCombinerMRJob driver = new AggregateCombinerMRJob();
		driver.setConf(conf);

		int exitCode = driver
				.run(new String[] { inputFilePath, outputDirPath });
		Assert.assertEquals(0, exitCode);

		FileSystem fs = FileSystem.get(URI.create(outputDirPath), conf);
		FileStatus[] listStatus = fs.listStatus(new Path(outputDirPath));
		Path[] paths = FileUtil.stat2Paths(listStatus);
		try {
			for (Path path : paths) {

				Assert.assertThat(path.getName(), Matchers.anyOf(
						startsWith("_SUCCESS"), startsWith("part")));
			}

		} finally {
			HDFSOperations.deleteFile(conf, outputDirPath);
		}

	}
	
	@Test
	public void SplitByMonthMRJobTest() throws Exception{
		Configuration conf = CommonOperations.getLocalConfiguration();
		String inputFilePath = CommonOperations
				.getClassPathFileAbsPath("airline/airline-sample.csv");
		String outputDirPath = CommonOperations
				.getFindableTempPath("SplitByMonthMR");

		SplitByMonthMRJob driver = new SplitByMonthMRJob();
		driver.setConf(conf);

		int exitCode = driver
				.run(new String[] { inputFilePath, outputDirPath });
		Assert.assertEquals(0, exitCode);

		FileSystem fs = FileSystem.get(URI.create(outputDirPath), conf);
		FileStatus[] listStatus = fs.listStatus(new Path(outputDirPath));
		Path[] paths = FileUtil.stat2Paths(listStatus);
		try {
			for (Path path : paths) {

				Assert.assertThat(path.getName(), Matchers.anyOf(
						startsWith("_SUCCESS"), startsWith("part")));
			}

		} finally {
			HDFSOperations.deleteFile(conf, outputDirPath);
		}
	}

}
