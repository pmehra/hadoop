package com.practice.hadoop;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Consumer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.MiniDFSCluster;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FileSystemMiniClusterTest {

	MiniDFSCluster miniDFSCluster;
	FileSystem fs;

	@Before
	public void setUp() throws Exception {
		Configuration conf = new Configuration();
		if (System.getProperty("test.build.data") == null) {
			System.setProperty("test.build.data", "/tmp");
		}

		miniDFSCluster = new MiniDFSCluster.Builder(conf).build();
		fs = miniDFSCluster.getFileSystem();
		FSDataOutputStream out = fs.create(new Path("/dir/file"));
		out.write("content".getBytes("UTF-8"));
		out.close();
	}

	@After
	public void tearDown() {
		Optional.ofNullable(fs).ifPresent(close(fs));
		Optional.ofNullable(miniDFSCluster).ifPresent(MiniDFSCluster::shutdown);
	}

	@Test(expected = FileNotFoundException.class)
	public void checkFileCreationNegative() throws Exception {
		fs.getFileStatus(new Path("no-such-file"));
	}

	@Test
	public void getFileStatusForAFileTest() throws Exception {
		FileStatus fileStatus = fs.getFileStatus(new Path("/dir/file"));

		assertThat(fileStatus.getPath().toUri().getPath(), is("/dir/file"));
		assertThat(fileStatus.isDirectory(), is(false));
		assertThat(fileStatus.getLen(), is(7L));
		assertThat(fileStatus.getModificationTime(),
				is(lessThanOrEqualTo(System.currentTimeMillis())));
		assertThat(fileStatus.getReplication(), is((short) 1));
		assertThat(fileStatus.getOwner(), is(System.getProperty("user.name")));
		assertThat(fileStatus.getGroup(), is("supergroup"));
		assertThat(fileStatus.getPermission().toString(), is("rw-r--r--"));

	}



	Consumer<Closeable> close(Closeable closebale) {
		return (Closeable c) -> {
			try {
				c.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		};
	}
}
