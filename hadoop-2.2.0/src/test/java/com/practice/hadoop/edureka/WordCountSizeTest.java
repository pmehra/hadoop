package com.practice.hadoop.edureka;

import static com.practice.hadoop.edureka.WordCountSizeReducer.formatedVerbiage;
import static org.hamcrest.CoreMatchers.startsWith;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import com.practice.hadoop.HDFSOperations;
import com.practice.hadoop.CommonOperations;

public class WordCountSizeTest {

	private static final String LINE = "I am a Line";

	@Test
	public void wordSizeMapperTest() throws IOException {

		Text value = new Text(LINE);
		new MapDriver<LongWritable, Text, IntWritable, Text>(
				new WordCountSizeMapper())
				.withInput(new LongWritable(1), value)
				.withAllOutput(outputPairs()).runTest();

	}

	@Test
	public void wordSizeReducerTest() throws IOException {
		new ReduceDriver<IntWritable, Text, IntWritable, Text>(
				new WordCountSizeReducer())
				.withInput(new IntWritable(1),
						Arrays.asList(new Text("I"), new Text("a")))
				.withOutput(new IntWritable(1),
						new Text(formatedVerbiage(1, "I,a", 2))).runTest();
	}

	@Test
	public void wordSizeReducerSingleInputTest() throws IOException {
		new ReduceDriver<IntWritable, Text, IntWritable, Text>(
				new WordCountSizeReducer())
				.withInput(new IntWritable(4), Arrays.asList(new Text("Line")))
				.withOutput(new IntWritable(4),
						new Text(formatedVerbiage(4, "Line", 1))).runTest();
	}

	@Test
	public void wordSizeDriverLocalTest() throws Exception {
		Configuration conf = new Configuration();
		conf.addResource("hadoop-local.xml");

		String inputFilePath = CommonOperations
				.getClassPathFileAbsPath("edureka/alphabets-minimal.txt");
		String outputDirPath = CommonOperations.getFindableTempPath("WordCountSize");

		WordCountSizeDriver driver = new WordCountSizeDriver();
		driver.setConf(conf);

		int exitCode = driver
				.run(new String[] { inputFilePath, outputDirPath });
		Assert.assertEquals(0, exitCode);

		FileSystem fs = FileSystem.get(URI.create(outputDirPath), conf);
		FileStatus[] listStatus = fs.listStatus(new Path(outputDirPath));
		Path[] paths = FileUtil.stat2Paths(listStatus);
		try {
			for (Path path : paths) {

				Assert.assertThat(path.getName(), Matchers.anyOf(
						startsWith("_SUCCESS"), startsWith("part")));
			}

		} finally {
			HDFSOperations.deleteFile(conf, outputDirPath);
		}

	}

	private static List<Pair<IntWritable, Text>> outputPairs() {
		StringTokenizer tokenizer = new StringTokenizer(LINE);
		List<Pair<IntWritable, Text>> pairs = new ArrayList<Pair<IntWritable, Text>>();
		Pair<IntWritable, Text> pair = null;
		String nextToken = null;
		while (tokenizer.hasMoreTokens()) {
			nextToken = tokenizer.nextToken();
			pair = new Pair<IntWritable, Text>(new IntWritable(
					nextToken.length()), new Text(nextToken));
			pairs.add(pair);
		}
		return pairs;
	}
}
