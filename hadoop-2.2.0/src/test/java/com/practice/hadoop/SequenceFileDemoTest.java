package com.practice.hadoop;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Reader;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.util.ReflectionUtils;
import org.junit.Test;

import com.practice.hadoop.io.AirlineRecordWritable;

public class SequenceFileDemoTest {

	private static final String[] DATA = { "One, two, buckle my shoe",
			"Three, four, shut the door", "Five, six, pick up sticks",
			"Seven, eight, lay them straight", "Nine, ten, a big fat hen" };

	@Test
	public void testSequenceFile() throws IllegalArgumentException, IOException {
		String uri = CommonOperations.getFindableTempPath("SeqFile") + ".seq";
		FileSystem localFS = CommonOperations.getLocalFS(uri);
		writeSequenceFile(uri, localFS);
		System.out.println(FileUtils.readFileToString(new File(uri)));
	}

	@Test
	public void sequenceFileWriteTest() throws Exception {
		Configuration conf = CommonOperations.getLocalConfiguration();
		FileSystem fs = FileSystem.get(conf);

		String filePath = CommonOperations.getFindableTempPath("SqTest");
		Path path = new Path(filePath);

		Writer writer = SequenceFile.createWriter(fs, conf, path,
				LongWritable.class, AirlineRecordWritable.class);

		AirlineRecordWritable record = new AirlineRecordWritable();
		record.carrier = new Text("AI");
		record.date = new Text("1/12/15");
		record.day = new Text("Tue");
		record.destination = new Text("India");
		record.month = new Text("Mar");
		record.origin = new Text("USA");
		record.year = new Text("2015");

		writer.append(new LongWritable(1), record);
		writer.close();

		String content = FileUtils.readFileToString(new File(filePath));
		System.out.println(content);

		Reader reader = new SequenceFile.Reader(fs, path, conf);
		AirlineRecordWritable readRecord = new AirlineRecordWritable();
		LongWritable readKey = new LongWritable();
		reader.next(readKey, readRecord);
		reader.close();
		
		System.out.println(readRecord);
		System.out.println(readKey);

		HDFSOperations.deleteFile(conf, filePath);
	}

	@Test
	public void readSequenceFileTest() throws Exception {
		String uri = CommonOperations.getFindableTempPath("SeqFile") + ".seq";
		FileSystem localFS = CommonOperations.getLocalFS(uri);
		writeSequenceFile(uri, localFS);

		Reader reader = new SequenceFile.Reader(localFS, new Path(uri),
				localFS.getConf());
		Writable key = (Writable) ReflectionUtils.newInstance(
				reader.getKeyClass(), localFS.getConf());
		Writable value = (Writable) ReflectionUtils.newInstance(
				reader.getValueClass(), localFS.getConf());
		try {
			long position = reader.getPosition();
			while (reader.next(key, value)) {
				String syncSeen = reader.syncSeen() ? "*" : "";
				System.out.printf("[%s%s]\t%s\t%s\n", position, syncSeen, key,
						value);
				position = reader.getPosition(); // beginning of next record
			}
		} finally {
			Optional.ofNullable(reader).ifPresent(IOUtils::closeStream);
		}

	}

	private void writeSequenceFile(String uri, FileSystem localFS)
			throws IOException {
		Writer writer = null;
		try {
			writer = SequenceFile.createWriter(localFS, localFS.getConf(),
					new Path(uri), IntWritable.class, Text.class);

			IntWritable key = new IntWritable();
			Text value = new Text();

			for (int i = 0; i < 100; i++) {
				key.set(100 - i);
				value.set(DATA[i % DATA.length]);
				System.out.printf("[%s]\t%s\t%s\n", writer.getLength(), key,
						value);
				writer.append(key, value);
			}
		} finally {
			Optional.ofNullable(writer).ifPresent(IOUtils::closeStream);
		}
	}

}
