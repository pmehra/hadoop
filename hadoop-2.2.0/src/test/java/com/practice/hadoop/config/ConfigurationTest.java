package com.practice.hadoop.config;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.junit.Ignore;
import org.junit.Test;

public class ConfigurationTest {

	@Test
	public void loadConfigFilesTest() {
		Configuration conf = new Configuration();
		conf.addResource("config/conf1.xml");

		assertThat(conf.getInt("size", 0), is(10));
	}

	/**
	 * Config loaded at last will take precedence.
	 */
	@Test
	public void configPrecedenceTest() {
		Configuration conf = new Configuration();
		conf.addResource("config/conf1.xml");
		conf.addResource("config/conf2.xml");

		assertThat(conf.getInt("size", 0), is(12));

	}

	@Test
	public void configFinalNeverChangeTest() {
		Configuration conf = new Configuration();
		conf.addResource("config/conf1.xml");
		conf.addResource("config/conf2.xml");

		assertThat(conf.get("weight"), is("heavy"));
	}

	/**
	 * ${heavy}.${weight} works in xml file
	 */
	@Test
	public void propertyResolverTest() {
		Configuration conf = new Configuration();
		conf.addResource("config/conf1.xml");

		assertThat(conf.get("size-weight"), is("10,heavy"));
	}

	@Test
	public void printConfigurationTest() throws Exception{
		ConfPrinter.main(new String[]{"-conf","hadoop-localhost.xml"});
	}

	/**
	 * Not working, as per book system prop should take precedence, but here
	 * resource file value is shown
	 */
	@Test
	@Ignore
	public void systemPropertiesPrecedenceTest() {
		System.setProperty("size", "14");

		Configuration conf = new Configuration();
		conf.addResource("config/conf1.xml");

		System.setProperty("size", "14");
		assertThat(conf.getInt("size", 0), is(14));

	}

	static class ConfPrinter extends Configured implements Tool {
		static{
			Configuration.addDefaultResource("yarn-site.xml");
		}

		@Override
		public int run(String[] args) throws Exception {
			Configuration conf = getConf();
			for (Entry<String, String> entry : conf) {
				System.out.println(entry.getKey() + " --> " + entry.getValue());
			}
			System.out.println(conf.size());
			return 0;
		}

		public static void main(String[] args) throws Exception {
			ToolRunner.run(new ConfPrinter(), args);
			System.exit(0);
		}

	}

}
