package com.practice.hadoop.ncdc;

import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Test;

public class MaxTemperatureTest {

	/**
	 * 006701199099999<b>1950</b>051507004+68750+023550FM-12+038299999
	 * V0203301N00671220001CN9999999N9<b>+0011</b>1+99999999999 year,temp
	 */
	public static String validRecord = "0067011990999991950051507004+68750+023550FM-12+038299999V0203301N00671220001CN9999999N9+00111+99999999999";

	/**
	 * missing is +9999 0067011990999991950051507004+68750+023550FM-12+038299999
	 * V0203301N00671220001CN9999999N9<b>+9999</b>1+99999999999
	 */
	public static String missingTempRecord = "0067011990999991950051507004+68750+023550FM-12+038299999V0203301N00671220001CN9999999N9+99991+99999999999";

	/**
	 * 0029029810999991901120820004+59500+020350FM-12+002699999
	 * V0201401N008219999999N0000001N9
	 * +99999+99999098521ADDGF108991999999999999999999
	 */
	private static String tempOver100 = "0029029810999991901120820004+59500+020350FM-12+002699999V0201401N008219999999N0000001N9+99999+99999098521ADDGF108991999999999999999999";

	@Test
	public void processValidRecordTest() throws IOException {

		Text line = new Text(validRecord);
		new MapDriver<LongWritable, Text, Text, IntWritable>()
				.withMapper(new MapperV1())
				.withInput(new LongWritable(1), line)
				.withOutput(new Text("1950"), new IntWritable(11)).runTest();

	}

	@Test
	public void missingTempRecordTest() throws IOException {
		Text line = new Text(missingTempRecord);

		new MapDriver<LongWritable, Text, Text, IntWritable>()
				.withMapper(new MapperV1())
				.withInput(new LongWritable(1), line).runTest();
	}

	@Test
	public void returnMaxIntegerReducerText() throws IOException {
		new ReduceDriver<Text, IntWritable, Text, IntWritable>()
				.withReducer(new ReducerV1())
				.withInput(new Text("1950"),
						Arrays.asList(new IntWritable(10), new IntWritable(11)))
				.withOutput(new Text("1950"), new IntWritable(11)).runTest();
	}

	public static class ReducerV1 extends
			Reducer<Text, IntWritable, Text, IntWritable> {

		@Override
		protected void reduce(Text key, Iterable<IntWritable> values,
				Reducer<Text, IntWritable, Text, IntWritable>.Context context)
				throws IOException, InterruptedException {
			int maxvalue = (int) Integer.MIN_VALUE;

			for (IntWritable val : values) {
				maxvalue = Math.max(val.get(), maxvalue);
			}

			context.write(key, new IntWritable(maxvalue));
		}

	}

	public static class MapperV1 extends
			Mapper<LongWritable, Text, Text, IntWritable> {

		@Override
		protected void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			String line = value.toString();
			String year = line.substring(15, 19);
			String tempStr = line.substring(87, 92);
			if (!isTempMissing(tempStr)) {
				int temp = Integer.parseInt(tempStr);
				context.write(new Text(year), new IntWritable(temp));
			}
		}

		private boolean isTempMissing(String tempStr) {
			return tempStr != null && tempStr.equalsIgnoreCase("+9999");
		}

	}

}
