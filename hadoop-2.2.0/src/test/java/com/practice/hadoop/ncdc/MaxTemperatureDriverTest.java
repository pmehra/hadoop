package com.practice.hadoop.ncdc;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.junit.Assert;
import org.junit.Test;

import com.practice.hadoop.HDFSOperations;

public class MaxTemperatureDriverTest {

	@Test
	public void testDriver() throws Exception {

		Configuration conf = new Configuration();
		conf.set("fs.default.name", "file:///");
		conf.set("mapreduce.framework.name", "local");

		String inputPath = ClassLoader.getSystemClassLoader()
				.getResource("ncdc/sample.txt").getPath();

		Path in = new Path(inputPath);
		Path out = new Path(UUID.randomUUID().toString());
		try {
			FileSystem fs = FileSystem.get(URI.create(out.toString()), conf);

			if (fs.exists(out)) {
				fs.delete(out, true);
			}

			MaxTemperatureDriver maxTempDriver = new MaxTemperatureDriver();
			maxTempDriver.setConf(conf);

			int exitCode = maxTempDriver.run(new String[] { in.toString(),
					out.toString() });

			assertEquals(0, exitCode);

			FileStatus[] listStatus = fs.listStatus(out);
			Path[] paths = FileUtil.stat2Paths(listStatus);
			for (Path filePath : paths) {
				System.out.println(filePath.toString());
			}
		} finally {
			HDFSOperations.deleteFile(conf, out.toString());
		}

	}

	@Test
	public void testDriverWithLocalRealmFile() throws Exception {

		Configuration conf = new Configuration();
		conf.addResource("hadoop-local.xml");

		String inputPath = ClassLoader.getSystemClassLoader()
				.getResource("ncdc/sample.txt").getPath();

		Path in = new Path(inputPath);
		Path out = new Path(UUID.randomUUID().toString());
		try {
			FileSystem fs = FileSystem.get(URI.create(out.toString()), conf);

			if (fs.exists(out)) {
				fs.delete(out, true);
			}

			MaxTemperatureDriver maxTempDriver = new MaxTemperatureDriver();
			maxTempDriver.setConf(conf);

			int exitCode = maxTempDriver.run(new String[] { in.toString(),
					out.toString() });

			assertEquals(0, exitCode);

			FileStatus[] listStatus = fs.listStatus(out);
			Path[] paths = FileUtil.stat2Paths(listStatus);
			for (Path filePath : paths) {
				System.out.println(filePath.toString());
			}
		} finally {
			HDFSOperations.deleteFile(conf, out.toString());
		}

	}

	@Test
	public void findPathToJarTEst(){
		File f=new File("target");
		System.out.println(f.exists());
		System.out.println(f.getAbsolutePath());
		System.out.println(Arrays.toString(f.list()));
	}

	/**
	 * The idea here is
	 * <ul>
	 * <li>Copy a file from local FS to HDFS PDM using UUID folder structure</li>
	 * <li>Run MR Job</li>
	 * <li>Clean up folders in HDFS</li>
	 * </ul>
	 * 
	 * But problem here is, although the classpath of client(test case) is set,
	 * but classpath of tasknode are not set, so it complains about the driver
	 * class not found at runtime. We need a way to send the jar file using this
	 * test-case.
	 */
	@Test
	public void testDriverWithRealmFile() throws Exception {

		String inputHdfs = "/" + UUID.randomUUID().toString();
		String outputHdfs = "/" + UUID.randomUUID().toString();
		String fileNameInHdfs = "sample.txt";

		Configuration conf = new Configuration();
		conf.addResource("hadoop-localhost.xml");
		try {
			HDFSOperations.copyFromLocal(conf, "ncdc/sample.txt", inputHdfs,
					fileNameInHdfs);

			MaxTemperatureDriver maxTempDriver = new MaxTemperatureDriver();
			maxTempDriver.setConf(conf);
			
			File f =new File("target/hadoop-2.2.0-1.0.jar");
			Assert.assertTrue(f.exists());
			// try to job.setJar to run this test
			// job.addFileToClassPath(file); adds in distributed cache

			int exitCode = maxTempDriver.run(new String[] {
					inputHdfs + "/sample.txt", outputHdfs, "file://"+f.getAbsolutePath() });

			Path outHdfs = new Path(outputHdfs);
			FileSystem fs = FileSystem.get(URI.create(outputHdfs), conf);
			FileStatus[] listStatus = fs.listStatus(outHdfs);
			Path[] paths = FileUtil.stat2Paths(listStatus);
			for (Path filePath : paths) {
				System.out.println(filePath.toString());
			}
		} finally {
			HDFSOperations.deleteFile(conf, inputHdfs);
			HDFSOperations.deleteFile(conf, outputHdfs);
		}
		Assert.fail("Because the jar is not available at task node classpath, so ClassNotFound exception");
	}

}
