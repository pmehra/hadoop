package com.practice.hadoop;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.util.Progressable;
import org.junit.Assert;
import org.junit.Test;

public class FileSystemTest {

	@Test
	public void writeFileOnHDFSTest() throws Exception {
		String fileName = "writeFileOnHDFSTest" + UUID.randomUUID().toString();
		String inputFilePath = CommonOperations
				.getClassPathFileAbsPath("edureka/alphabets-minimal.txt");

		InputStream in = new BufferedInputStream(new FileInputStream(
				inputFilePath));
		FileSystem localhostFS = CommonOperations.getLocalhostFS(fileName);

		FSDataOutputStream out = localhostFS.create(new Path(fileName),
				new Progressable() {

					@Override
					public void progress() {
						System.out.print(".");
					}
				});

		IOUtils.copyBytes(in, out, 4096, true);

		Assert.assertTrue(localhostFS.exists(new Path(fileName)));

		localhostFS.delete(new Path(fileName), true);
	}

	@Test
	public void createDirectoryTest() throws Exception {
		Configuration conf = new Configuration();
		conf.addResource("hadoop-localhost.xml");

		FileSystem fs = FileSystem.get(URI.create("/user/pmehra"), conf);
		fs.mkdirs(new Path("folder"));

		Assert.assertTrue(fs.exists(new Path("folder")));

		fs.delete(new Path("folder"), true);
	}

	/**
	 * This test run on Linux and hadoop should be installed and running.
	 * hdfs-site.xml should be on classpath
	 * 
	 * @throws Exception
	 */
	@Test
	public void readingAfileFromHDFS() throws Exception {
		String uri = "/input";
		Configuration conf = new Configuration();
		conf.addResource("hadoop-localhost.xml");
		FileSystem fs = FileSystem.get(URI.create(uri), conf);
		InputStream in = null;
		try {
			in = fs.open(new Path(uri + "/ncdc/text.txt"));
			IOUtils.copyBytes(in, System.out, 4096, false);
		} finally {
			IOUtils.closeStream(in);
		}
	}

	/**
	 * hdfs:// uri denotes HDFS file system and it look for hdfs deamon
	 * 
	 * @throws Exception
	 */
	@Test
	public void readingAfileFromHDFSWithoutRealmXMl() throws Exception {
		String uri = "hdfs://localhost:9000/input/ncdc/text.txt";
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(URI.create(uri), conf);
		InputStream in = null;
		try {
			in = fs.open(new Path(uri));
			IOUtils.copyBytes(in, System.out, 4096, false);
		} finally {
			IOUtils.closeStream(in);
		}
	}

	/**
	 * File:// denotest local filesystem
	 */
	@Test
	public void readingAfileFromLocalWithoutRealmXMl() throws Exception {
		String uri = "file:///home/pmehra/data/hadoop-data/input/hadoop-practice/test-data/ch1/file1.txt";
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(URI.create(uri), conf);
		InputStream in = null;
		try {
			in = fs.open(new Path(uri));
			IOUtils.copyBytes(in, System.out, 4096, false);
		} finally {
			IOUtils.closeStream(in);
		}
	}

	@Test
	public void readFileTwiceUsingSeek() throws Exception {
		String uri = "/std-input.txt";
		FileSystem fs = CommonOperations.getLocalhostFS(uri);
		FSDataInputStream in = null;
		try {
			in = fs.open(new Path(uri));
			IOUtils.copyBytes(in, System.out, 4096, false);
			in.seek(0);
			IOUtils.copyBytes(in, System.out, 4096, false);
		} finally {
			IOUtils.closeStream(in);
		}
	}

	@Test
	public void listFileStatusTest() throws Exception {
		FileSystem localhostFS = CommonOperations
				.getLocalhostFS("/user/pmehra");
		PathFilter pf = (Path p) -> p.getName().endsWith(".txt");
		
		FileStatus[] listStatus = localhostFS.listStatus(new Path("/user/pmehra"), pf);
		for (FileStatus fileStatus : listStatus) {
			System.out.println(fileStatus.getPath().toUri().getPath());
		}
		
		listStatus = localhostFS.listStatus(new Path("/user/pmehra"));
		for (FileStatus fileStatus : listStatus) {
			System.out.println(fileStatus.getPath().toUri().getPath());
		}
		
		PathFilter pf2 = (Path p) -> p.toString().matches("\"*.txt\"");
		listStatus = localhostFS.listStatus(new Path("/user/pmehra"), pf2);
		for (FileStatus fileStatus : listStatus) {
			System.out.println(fileStatus.getPath().toUri().getPath());
		}
		
		
		localhostFS.close();
	}
}
