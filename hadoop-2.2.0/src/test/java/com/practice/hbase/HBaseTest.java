package com.practice.hbase;

import java.io.IOException;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.io.compress.Compression.Algorithm;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import com.practice.hadoop.CommonOperations;

public class HBaseTest {

	static Admin admin = CommonOperations.getHBaseLocal();

	@AfterClass
	public static void tearDown() throws Exception {
		admin.close();
	}

	@Test
	public void createTableTest() throws Exception {

		TableName employee = TableName.valueOf("employee");
		String columnFamily = "info";

		Table table = createTable(employee, columnFamily);

		Assert.assertTrue(admin.tableExists(employee));
		Assert.assertEquals(columnFamily, admin.getTableDescriptor(employee)
				.getColumnFamilies()[0].getNameAsString());

		deleteTable(table);
	}

	@Test
	public void addRowsTest() throws Exception {
		TableName employee = TableName.valueOf("employee");
		String columnFamily = "info";
		Table table = createTable(employee, columnFamily);

		byte[] family = Bytes.toBytes(columnFamily);
		byte[] row = Bytes.toBytes("1");
		byte[] column = Bytes.toBytes("address");
		byte[] value = Bytes.toBytes("New York");

		Put put = new Put(row);
		put.addColumn(family, column, value);
		table.put(put);

		Get getRow = new Get(row);
		Result result = table.get(getRow);

		byte[] addressValue = result.getValue(family, column);
		String strValue = Bytes.toString(addressValue);
		Assert.assertEquals("New York", strValue);
		deleteTable(table);
	}

	@Test
	public void scanRowsTest() throws Exception {
		TableName employee = TableName.valueOf("employee");
		String columnFamily1 = "info";
		String columnFamily2 = "work";
		Table table = createTable(employee, columnFamily1, columnFamily2);

		byte[] family = Bytes.toBytes(columnFamily1);
		byte[] row = Bytes.toBytes("1");
		byte[] column = Bytes.toBytes("address");
		byte[] value = Bytes.toBytes("New York");

		Put put = new Put(row);
		put.addColumn(family, column, value);
		table.put(put);

		byte[] family2 = Bytes.toBytes(columnFamily2);
		byte[] column2 = Bytes.toBytes("skills");
		byte[] value2 = Bytes.toBytes("java");

		put = new Put(row);
		put.addColumn(family2, column2, value2);
		table.put(put);

		Scan scan = new Scan(row);
		ResultScanner scanRS = table.getScanner(scan);

		boolean flag1 = false, flag2 = false;
		for (Result rs : scanRS) {
			if (rs.containsColumn(family, column)) {
				Assert.assertEquals("New York",
						Bytes.toString(rs.getValue(family, column)));
				flag1 = true;
			}
			if (rs.containsColumn(family2, column2)) {
				Assert.assertEquals("java",
						Bytes.toString(rs.getValue(family2, column2)));
				flag2 = true;
			}
		}
		Assert.assertTrue(flag1);
		Assert.assertTrue(flag2);

		deleteTable(table);

	}

	@Test
	public void simpleRowCounterTest() throws Exception {

		TableName tableName = TableName.valueOf("employee");
		String columnFamily = "info";
		Table table = createTable(tableName, columnFamily);

		int num = 5;
		addRows(columnFamily, table, num);
		SimpleRowCounter.main(new String[] { "employee" });
		deleteTable(table);
	}

	private void addRows(String columnFamily, Table table, int num)
			throws IOException {
		for (int i = 0; i < num; i++) {
			byte[] family = Bytes.toBytes(columnFamily);
			byte[] row = Bytes.toBytes("row-"+i);
			byte[] column = Bytes.toBytes("address");
			byte[] value = Bytes.toBytes("New York-"+i);

			Put put = new Put(row);
			put.addColumn(family, column, value);
			table.put(put);
		}
	}

	private void deleteTable(Table table) throws IOException {
		TableName tableName = table.getTableDescriptor().getTableName();
		admin.disableTable(tableName);
		admin.deleteTable(tableName);
		table.close();
	}

	private Table createTable(TableName tableName, String... columnFamilies)
			throws IOException {
		HTableDescriptor tableDesc = new HTableDescriptor(tableName);
		for (String columnFamily : columnFamilies) {
			tableDesc.addFamily(new HColumnDescriptor(columnFamily)
					.setCompressionType(Algorithm.SNAPPY));

		}
		admin.createTable(tableDesc);
		return admin.getConnection().getTable(tableName);
	}

}
