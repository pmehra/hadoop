package com.practice.hadoop;

public enum CodecType {
	GZIP("gzip");

	private String value;

	private CodecType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
