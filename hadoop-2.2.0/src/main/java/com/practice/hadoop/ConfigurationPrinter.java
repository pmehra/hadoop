package com.practice.hadoop;

import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Print all the properties. Realm can be used using -conf option and pointing
 * hadoop-local.xml, hadoop-localhost.xml (pseudodistributed) &
 * hadoop-anycluster.xml
 *
 * export HADOOP_CLASSPATH=code/target/classes
 * hadoop com.practice.hadoop.ConfigurationPrinter | grep fs.defaultFS
 * 
 * @author pmehra
 */
public class ConfigurationPrinter extends Configured implements Tool {

	static {
		/*
		 * Configuration.addDefaultResource("hdfs-default.xml");
		 * Configuration.addDefaultResource("hdfs-site.xml");
		 * Configuration.addDefaultResource("mapred-default.xml");
		 * Configuration.addDefaultResource("mapred-site.xml");
		 */
	}

	private String format = "%s=%s\n";

	@Override
	public int run(String[] args) throws Exception {
		Configuration conf = getConf();

		for (Entry<String, String> entry : conf) {
			System.out.printf(String.format(format, entry.getKey(),
					entry.getValue()));
		}
		return 0;
	}

	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new ConfigurationPrinter(), args);
		System.exit(exitCode);
	}

}
