package com.practice.hadoop;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.GenericOptionsParser;

public class DriverUtil {

	public static IOPaths getIOPaths(String[] args) throws IOException {
		args = new GenericOptionsParser(args).getRemainingArgs();
		if (args.length < 2) {
			GenericOptionsParser.printGenericCommandUsage(System.err);
			throw new IllegalStateException("path is not set ");
		}
		return new IOPaths(args[0], args[1]);

	}

	public static class IOPaths {
		final String input;
		final String output;

		public IOPaths(String input, String output) {
			this.input = input;
			this.output = output;
		}

		public Path getInput() {
			return new Path(input);
		}

		public Path getOutput() {
			return new Path(output);
		}
	
	}

}
