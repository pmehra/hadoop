package com.practice.hadoop.ncdc;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MaxTemperatureReducer extends
		Reducer<Text, IntWritable, Text, IntWritable> {

	@Override
	protected void reduce(Text key, Iterable<IntWritable> values,
			Reducer<Text, IntWritable, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {

		int maxvalue = (int) Integer.MIN_VALUE;

		for (IntWritable val : values) {
			maxvalue = Math.max(val.get(), maxvalue);
		}

		context.write(key, new IntWritable(maxvalue));
	}

}
