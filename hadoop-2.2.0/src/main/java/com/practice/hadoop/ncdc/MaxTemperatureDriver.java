package com.practice.hadoop.ncdc;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class MaxTemperatureDriver extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {

		boolean flag = false;
		if (args.length < 2) {
			System.err
					.println("Usage: MaxTemperature <input path> <output path>, using local mode now");
			ToolRunner.printGenericCommandUsage(System.err);
			flag = true;
		}

		Job job = new Job(getConf());
		job.setJarByClass(MaxTemperatureDriver.class);
		job.setJobName("Max temperature");
		if (args.length > 2) {
			job.addFileToClassPath(new Path(args[2]));
		}

		if (flag) {
			FileInputFormat
					.addInputPath(
							job,
							new Path(
									"/home/pmehra/learning/hadoop/code/hadoop/input/ncdc/sample.txt"));
			FileOutputFormat
					.setOutputPath(
							job,
							new Path(
									"/home/pmehra/learning/hadoop/code/hadoop/hadoop-2.2.0/output/op"));
		} else {
			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileOutputFormat.setOutputPath(job, new Path(args[1]));
		}

		job.setMapperClass(MaxTemperatureMapper.class);
		job.setReducerClass(MaxTemperatureReducer.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		return job.waitForCompletion(true) ? 0 : 1;

	}

	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new MaxTemperatureDriver(), args);
		System.exit(exitCode);
	}

}
