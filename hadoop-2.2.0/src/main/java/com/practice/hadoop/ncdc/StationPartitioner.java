package com.practice.hadoop.ncdc;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class StationPartitioner extends Partitioner<LongWritable, Text> {

	private NcdcRecordParser parser = new NcdcRecordParser();

	@Override
	public int getPartition(LongWritable key, Text value, int numPartitions) {
		return Math.abs(parser.parse(value).getStationIdAsInt())
				% numPartitions;
	}
}
