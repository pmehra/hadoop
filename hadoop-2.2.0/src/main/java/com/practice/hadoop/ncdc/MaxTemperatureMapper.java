package com.practice.hadoop.ncdc;

import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MaxTemperatureMapper extends
		Mapper<LongWritable, Text, Text, IntWritable> {

	private static final int MISSING = 9999;

	enum Temperature {
		OVER_100
	}

	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {

		String line = value.toString();
		String year = line.substring(15, 19);
		int airTemperature;

		if (line.charAt(87) == '+') { // parseInt doesn't like leading plus
										// signs
			airTemperature = Integer.parseInt(line.substring(88, 92));
		} else {
			airTemperature = Integer.parseInt(line.substring(87, 92));
		}

		String quality = line.substring(92, 93);
		System.out.println("Input splits are "
				+ Arrays.toString(context.getInputSplit().getLocations()));

		if (airTemperature != MISSING && quality.matches("[01459]")) {
			if (airTemperature > 1000) {
				System.err
						.println("Temperature is over 100 for input map value : "
								+ value);
				context.setStatus("Detected possibly corrupted record, see logs.");
				context.getCounter(Temperature.OVER_100).increment(1);
			}
			context.write(new Text(year), new IntWritable(airTemperature));
		}
	}

}
