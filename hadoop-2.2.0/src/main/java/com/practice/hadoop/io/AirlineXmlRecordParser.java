package com.practice.hadoop.io;

import org.apache.commons.lang.StringUtils;

import com.practice.hadoop.airline.AirlineRecordParser;

public class AirlineXmlRecordParser {

	private AirlineRecordParser record;

	public AirlineXmlRecordParser(AirlineRecordParser record) {
		if (record == null) {
			throw new IllegalArgumentException("record is null");
		}
		this.record = record;
	}

	public static String mainStartTag() {
		return "<recs> \n";
	}

	public static String mainEndTag() {
		return "</recs> \n";
	}

	private String startTag() {
		return "<rec> \n";
	}

	private String endTag() {
		return "</rec> \n";
	}

	public String toXmlRecord(long key) {
		StringBuilder builder = new StringBuilder(startTag());
		builder.append(getKey(key));
		builder.append(getYear());
		builder.append(getMonth());
		builder.append(getDay());
		builder.append(getDate());
		builder.append(getCarrier());
		builder.append(getDestination());
		builder.append(getOrigin());
		builder.append(endTag());
		return builder.toString();
	}

	private String getKey(long key) {
		return "\t<key>" + orDefault(key) + "</key> \n";
	}

	private String getYear() {
		return "\t<year>" + orDefault(record.getYear()) + "</year> \n";
	}

	private String getMonth() {
		return "\t<month>" + orDefault(record.getMonth()) + "</month> \n";
	}

	private String getDay() {
		return "\t<day>" + orDefault(record.getDay()) + "</day> \n";
	}

	private String getOrigin() {
		return "\t<origin>" + orDefault(record.getOrigin()) + "</origin> \n";
	}

	private String getDestination() {
		return "\t<destination>" + orDefault(record.getDestination())
				+ "</destination> \n";
	}

	private String getCarrier() {
		return "\t<carrier>" + orDefault(record.getCarrierCode()) + "</carrier> \n";
	}

	private String getDate() {
		return "\t<date>" + orDefault(record.getFlightDate()) + "</date> \n";
	}

	private <T> String orDefault(String value) {
		return StringUtils.isBlank(value) ? "null" : value;
	}

	private <T> String orDefault(int value) {
		return value < 1 ? "null" : Integer.toString(value);
	}

	private <T> String orDefault(long value) {
		return value < 1 ? "null" : Long.toString(value);
	}

}
