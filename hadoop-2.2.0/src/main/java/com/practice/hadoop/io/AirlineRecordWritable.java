package com.practice.hadoop.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

public class AirlineRecordWritable implements Writable {
	public Text year = new Text();
	public Text month = new Text();
	public Text day = new Text();
	public Text date = new Text();
	public Text carrier = new Text();
	public Text destination = new Text();
	public Text origin = new Text();

	public AirlineRecordWritable() {
	}

	public void setDelaysWritable(AirlineRecordWritable dw) {
		this.year = dw.year;
		this.month = dw.month;
		this.day = dw.day;
		this.date = dw.date;
		this.carrier = dw.carrier;
		this.destination = dw.destination;
		this.origin = dw.origin;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		this.year.write(out);
		this.month.write(out);
		this.day.write(out);
		this.date.write(out);
		this.carrier.write(out);
		this.destination.write(out);
		this.origin.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.year.readFields(in);
		this.month.readFields(in);
		this.day.readFields(in);
		this.date.readFields(in);
		this.carrier.readFields(in);
		this.destination.readFields(in);
		this.origin.readFields(in);
	}

	@Override
	public String toString() {
		return "AirlineRecordWritable [year=" + year + ", month=" + month
				+ ", day=" + day + ", date=" + date + ", carrier=" + carrier
				+ ", destination=" + destination + ", origin=" + origin + "]";
	}

}
