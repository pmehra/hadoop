package com.practice.hadoop.io;

import java.io.IOException;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.practice.hadoop.DriverUtil;
import com.practice.hadoop.DriverUtil.IOPaths;
import com.practice.hadoop.airline.AirlineRecordParser;

public class TextToXmlMRJob extends Configured implements Tool {

	public static class TextToXmlMapper extends
			Mapper<LongWritable, Text, LongWritable, Text> {

		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, LongWritable, Text>.Context context)
				throws IOException, InterruptedException {
			AirlineRecordParser record = new AirlineRecordParser(
					value.toString());
			if (record.isHeader()) {
				return;
			}

			/*
			 * AirlineXmlRecordParser xmlRecordParser = new
			 * AirlineXmlRecordParser( record);
			 * context.write(NullWritable.get(), new
			 * Text(xmlRecordParser.toXmlRecord(key.get())));
			 */
			context.write(key, value);
		}
	}

	
	@Override
	public int run(String[] args) throws Exception {
		IOPaths ioPaths = DriverUtil.getIOPaths(args);
		Job job = Job.getInstance(getConf());

		job.setJarByClass(TextToXmlMRJob.class);
		job.setMapperClass(TextToXmlMapper.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(XmlOutputFormat.class);
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);
		job.setNumReduceTasks(0);

		FileSystem.get(getConf()).delete(ioPaths.getOutput(), true);

		FileInputFormat.setInputPaths(job, ioPaths.getInput());
		FileOutputFormat.setOutputPath(job, ioPaths.getOutput());
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new TextToXmlMRJob(), args));
	}

}
