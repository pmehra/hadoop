package com.practice.hadoop.io;

import java.io.IOException;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import com.practice.hadoop.airline.AirlineRecordParser;

public class XmlOutputFormat extends FileOutputFormat<LongWritable, Text> {

	@Override
	public RecordWriter<LongWritable, Text> getRecordWriter(
			TaskAttemptContext job) throws IOException, InterruptedException {
		String extension = ".xml";
		Path path = getDefaultWorkFile(job, extension);
		FileSystem fs = FileSystem.get(job.getConfiguration());
		FSDataOutputStream fout = fs.create(path, true);
		return new XmlRecordWriter(fout);
	}

	public static class XmlRecordWriter extends
			RecordWriter<LongWritable, Text> {

		private FSDataOutputStream fout;

		public XmlRecordWriter(FSDataOutputStream fout) throws IOException {
			this.fout = fout;
			fout.write(AirlineXmlRecordParser.mainStartTag().getBytes());
		}

		@Override
		public void write(LongWritable key, Text value) throws IOException,
				InterruptedException {
			AirlineXmlRecordParser parser = new AirlineXmlRecordParser(
					new AirlineRecordParser(value.toString()));
			fout.write(parser.toXmlRecord(key.get()).getBytes());
		}

		@Override
		public void close(TaskAttemptContext context) throws IOException,
				InterruptedException {
			try {
				fout.write(AirlineXmlRecordParser.mainEndTag().getBytes());
			} finally {
				fout.close();
			}
		}

	}

}
