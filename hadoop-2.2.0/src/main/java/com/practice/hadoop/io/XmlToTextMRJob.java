package com.practice.hadoop.io;

import java.io.IOException;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.practice.hadoop.DriverUtil;
import com.practice.hadoop.DriverUtil.IOPaths;

public class XmlToTextMRJob extends Configured implements Tool {

	public static class XmlToTextMapper extends
			Mapper<LongWritable, AirlineRecordWritable, LongWritable, Text> {

		@Override
		protected void map(
				LongWritable key,
				AirlineRecordWritable value,
				Mapper<LongWritable, AirlineRecordWritable, LongWritable, Text>.Context context)
				throws IOException, InterruptedException {
			context.write(key, new Text(value.toString()));
		}
	}

	@Override
	public int run(String[] args) throws Exception {
		IOPaths ioPaths = DriverUtil.getIOPaths(args);
		Job job = Job.getInstance(getConf());

		job.setJarByClass(XmlToTextMRJob.class);
		job.setMapperClass(XmlToTextMapper.class);

		job.setInputFormatClass(XmlInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);

		job.setNumReduceTasks(0);

		FileSystem.get(getConf()).delete(ioPaths.getOutput(), true);

		FileInputFormat.setInputPaths(job, ioPaths.getInput());
		FileOutputFormat.setOutputPath(job, ioPaths.getOutput());
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new XmlToTextMRJob(), args));
	}

}
