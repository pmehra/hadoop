package com.practice.hadoop.airline.join;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class CarrierKey implements WritableComparable<CarrierKey> {

	public static enum CarrierType {
		KEY(new IntWritable(1)), DATA(new IntWritable(2)), UNKNOWN(
				new IntWritable(3));

		private IntWritable value;

		private CarrierType(IntWritable intWritable) {
			this.value = intWritable;
		}

		public IntWritable value() {
			return value;
		}
	}

	private IntWritable type = CarrierType.UNKNOWN.value();
	private Text code = new Text();
	private Text desc = new Text();

	public CarrierKey() {
	}

	public CarrierKey(IntWritable type, Text code, Text desc) {
		super();
		this.type = type;
		this.code = code;
		this.desc = desc;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		type.write(out);
		code.write(out);
		desc.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		type.readFields(in);
		code.readFields(in);
		desc.readFields(in);
	}

	@Override
	public int compareTo(CarrierKey o) {
		if (this.code.equals(o.code)) {
			return this.type.compareTo(o.type);
		}
		return this.code.compareTo(o.code);
	}

	/*
	 * This hascode should be ignored else partioner will not result in correct
	 * Reducer. Default partitioner #HashPartioner uses hascode implementation.
	 * We have to give custom implementation of hash code.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarrierKey other = (CarrierKey) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CK [" + type + "," + code + "," + desc + "]";
	}

	public IntWritable getType() {
		return type;
	}

	public void setType(IntWritable type) {
		this.type = type;
	}

	public Text getCode() {
		return code;
	}

	public void setCode(Text code) {
		this.code = code;
	}

	public Text getDesc() {
		return desc;
	}

	public void setDesc(Text desc) {
		this.desc = desc;
	}

}
