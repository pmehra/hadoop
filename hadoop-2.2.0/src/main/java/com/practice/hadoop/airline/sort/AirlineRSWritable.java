package com.practice.hadoop.airline.sort;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import com.practice.hadoop.airline.AirlineRecordParser;

public class AirlineRSWritable implements Writable {

	private IntWritable year = new IntWritable();
	private IntWritable month = new IntWritable();
	private Text date = new Text();
	private Text dayOfWeek = new Text();
	private IntWritable arrDelay = new IntWritable();
	private IntWritable depDelay = new IntWritable();
	private Text originAirportCode = new Text();
	private Text destAirportCode = new Text();
	private Text carrierCode = new Text();

	public AirlineRSWritable() {
	}

	public AirlineRSWritable(AirlineRecordParser parser) {
		if (parser == null) {
			throw new IllegalArgumentException("parser is null");
		}

		year = new IntWritable(parser.parseInt(parser.getYear()));
		month = new IntWritable(parser.parseInt(parser.getMonth()));
		date = new Text(parser.getFlightDate());
		dayOfWeek =  new Text(parser.getWeekDay());
		arrDelay = new IntWritable(parser.parseInt(parser.getArrivalDelay()));
		depDelay = new IntWritable(parser.parseInt(parser.getDepartureDelay()));
		originAirportCode = new Text(parser.getOrigin());
		destAirportCode = new Text(parser.getDestination());
		carrierCode = new Text(parser.getCarrierCode());
	}

	public void setDelaysWritable(AirlineRSWritable rs) {
		this.year = rs.year;
		this.month = rs.month;
		this.date = rs.date;
		this.dayOfWeek = rs.dayOfWeek;
		this.arrDelay = rs.arrDelay;
		this.depDelay = rs.depDelay;
		this.originAirportCode = rs.originAirportCode;
		this.destAirportCode = rs.destAirportCode;
		this.carrierCode = rs.carrierCode;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		this.year.write(out);
		this.month.write(out);
		this.date.write(out);
		this.dayOfWeek.write(out);
		this.arrDelay.write(out);
		this.depDelay.write(out);
		this.originAirportCode.write(out);
		this.destAirportCode.write(out);
		this.carrierCode.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.year.readFields(in);
		this.month.readFields(in);
		this.date.readFields(in);
		this.dayOfWeek.readFields(in);
		this.arrDelay.readFields(in);
		this.depDelay.readFields(in);
		this.originAirportCode.readFields(in);
		this.destAirportCode.readFields(in);
		this.carrierCode.readFields(in);
	}

	public IntWritable getYear() {
		return year;
	}

	public void setYear(IntWritable year) {
		this.year = year;
	}

	public IntWritable getMonth() {
		return month;
	}

	public void setMonth(IntWritable month) {
		this.month = month;
	}

	public Text getDate() {
		return date;
	}

	public void setDate(Text date) {
		this.date = date;
	}

	public Text getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(Text dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public IntWritable getArrDelay() {
		return arrDelay;
	}

	public void setArrDelay(IntWritable arrDelay) {
		this.arrDelay = arrDelay;
	}
	

	public IntWritable getDepDelay() {
		return depDelay;
	}

	public void setDepDelay(IntWritable depDelay) {
		this.depDelay = depDelay;
	}

	public Text getOriginAirportCode() {
		return originAirportCode;
	}

	public void setOriginAirportCode(Text originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	public Text getDestAirportCode() {
		return destAirportCode;
	}

	public void setDestAirportCode(Text destAirportCode) {
		this.destAirportCode = destAirportCode;
	}

	public Text getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(Text carrierCode) {
		this.carrierCode = carrierCode;
	}

}
