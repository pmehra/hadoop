package com.practice.hadoop.airline.sort;

import java.io.IOException;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.practice.hadoop.DriverUtil;
import com.practice.hadoop.DriverUtil.IOPaths;
import com.practice.hadoop.airline.AirlineRecordParser;

public class SecondarySortMRJob extends Configured implements Tool {

	public static class SecondarySortMapper extends
			Mapper<LongWritable, Text, AirlineCompositeKey, AirlineRSWritable> {

		@Override
		protected void map(
				LongWritable key,
				Text value,
				Mapper<LongWritable, Text, AirlineCompositeKey, AirlineRSWritable>.Context context)
				throws IOException, InterruptedException {
			AirlineRecordParser parser = new AirlineRecordParser(
					value.toString());

			if (parser.isHeader() || !parser.isArrivalDelay()) {
				return;
			}

			AirlineCompositeKey compositeKey = new AirlineCompositeKey(
					new Text(parser.getOrigin()), new Text(
							parser.getFlightDate()));
			AirlineRSWritable rsValue = new AirlineRSWritable(parser);
			context.write(compositeKey, rsValue);
		}

	}

	public static class SecondarySortReducer extends
			Reducer<AirlineCompositeKey, AirlineRSWritable, NullWritable, Text> {

		@Override
		protected void reduce(
				AirlineCompositeKey key,
				Iterable<AirlineRSWritable> value,
				Reducer<AirlineCompositeKey, AirlineRSWritable, NullWritable, Text>.Context context)
				throws IOException, InterruptedException {
			int previousDelays = 0;
			StringBuilder out = new StringBuilder(key.getAirportCode().toString());
			for (AirlineRSWritable airlineRSWritable : value) {
				out.append(" | " + airlineRSWritable.getDate().toString());
				previousDelays++;
			}
			out.append(" | " + previousDelays);
			context.write(NullWritable.get(), new Text(out.toString()));

		}

	}

	public static class SecondarySortPartioner extends
			Partitioner<AirlineCompositeKey, AirlineRSWritable> {

		@Override
		public int getPartition(AirlineCompositeKey key,
				AirlineRSWritable value, int numPartitions) {
			return Math.abs(key.hashCode() % numPartitions);
		}

	}

	@Override
	public int run(String[] args) throws Exception {
		IOPaths ioPaths = DriverUtil.getIOPaths(args);

		Job job = Job.getInstance(getConf());
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setJarByClass(SecondarySortMRJob.class);
		job.setMapOutputKeyClass(AirlineCompositeKey.class);
		job.setMapOutputValueClass(AirlineRSWritable.class);

		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setPartitionerClass(SecondarySortPartioner.class);
		job.setMapperClass(SecondarySortMapper.class);
		job.setReducerClass(SecondarySortReducer.class);
		job.setNumReduceTasks(4);
		job.setSortComparatorClass(SecondarySortComparator.class);
		job.setGroupingComparatorClass(GroupSortComparator.class);

		FileSystem.get(getConf()).delete(ioPaths.getOutput(), true);
		FileInputFormat.setInputPaths(job, ioPaths.getInput());
		FileOutputFormat.setOutputPath(job, ioPaths.getOutput());

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new SecondarySortMRJob(), args));
	}

}
