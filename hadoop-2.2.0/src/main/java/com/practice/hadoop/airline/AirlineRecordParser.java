package com.practice.hadoop.airline;

import java.util.StringJoiner;

import org.apache.commons.lang.StringUtils;

public class AirlineRecordParser {

	private static final String NA = "NA";
	private String[] recordArr;
	private static String[] days = new String[] { "Sun", "Mon", "Tue", "Wed",
			"Thu", "Fri", "Sat" };

	protected static String[] months = new String[] { "Jan", "Feb", "Mar",
			"Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	public AirlineRecordParser(String record) {
		if (StringUtils.isBlank(record)) {
			recordArr = new String[] {};
			return;
		}
		this.recordArr = record.split(",");
	}

	public boolean isHeader() {
		return get(0).equals("Year");
	}

	public String getYear() {
		return get(0);
	}

	public String getMonth() {
		return get(1);
	}

	public String getMonthAsText() {
		int index = parseInt(get(1));
		return index == -1 || index - 1 < 0 ? NA : months[index - 1];
	}

	public String getDay() {
		return get(2);
	}

	public String getDepartureTime() {
		return get(4);
	}

	public String getArrivalTime() {
		return get(6);
	}

	public String getOrigin() {
		return get(16);
	}

	public String getDestination() {
		return get(17);
	}

	public boolean isDiverted() {
		return parseBoolean(get(23));
	}

	public boolean isCancelled() {
		return parseBoolean(get(21));
	}

	public String getCarrierCode() {
		return get(8);
	}

	public String getDistance() {
		return get(18);
	}

	public String getElapsedTime() {
		return get(11);
	}

	public String getScheduleFlightTime() {
		return get(13);
	}

	public String getDepartureDelay() {
		return get(14);
	}

	public String getArrivalDelay() {
		return get(15);
	}

	public boolean isArrivalDelay() {
		return parseInt(getArrivalDelay()) > 0;
	}

	public boolean isDepatureDelay() {
		return parseInt(getDepartureDelay()) > 0;
	}

	public String getFlightDate() {
		StringJoiner joiner = new StringJoiner("/");
		joiner.add(getDay());
		joiner.add(getMonth());
		joiner.add(getYear());
		return joiner.toString();
	}

	public String getWeekDay() {
		int index = parseInt(get(3));
		return (index == -1 || index - 1 < 0) ? NA : days[index - 1];
	}

	public int getWeekDayAsInt() {
		return parseInt(get(3));
	}

	private String get(int index) {
		if (recordArr.length - 1 < index) {
			return NA;
		}
		return StringUtils.defaultIfEmpty(recordArr[index], NA);
	}

	public boolean parseBoolean(String value) {
		if (StringUtils.isBlank(value)) {
			return false;
		}
		return parseInt(value) == 1 ? true : false;
	}

	public int parseInt(String value) {
		if (StringUtils.isBlank(value)) {
			return -1;
		}
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return -1;
		}

	}

	public long parseLong(String value) {
		if (StringUtils.isBlank(value)) {
			return -1L;
		}
		try {
			return Long.parseLong(value);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return -1L;
		}
	}

	public String append(String... args) {
		StringJoiner joiner = new StringJoiner(",");
		for (String val : args) {
			if (StringUtils.isNotBlank(val)) {
				joiner.add(val);
			}
		}
		return joiner.toString();
	}

	public String selectBasicInfo() {
		StringJoiner joiner = new StringJoiner(",");
		joiner.add("carrier(" + getCarrierCode() + ")");
		joiner.add("flightDate(" + getFlightDate() + ")");
		joiner.add("origin(" + getOrigin() + ")");
		joiner.add("destination(" + getDestination() + ")");
		joiner.add("distance(" + getDistance() + ")");
		joiner.add("elapsedTime(" + getElapsedTime() + ")");
		return joiner.toString();
	}

}
