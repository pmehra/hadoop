package com.practice.hadoop.airline;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class AggregateByYearAirlineMapper extends Mapper<LongWritable, Text, IntWritable, IntWritable>{

	private static IntWritable ONE = new IntWritable(1);
	
	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, IntWritable, IntWritable>.Context context)
			throws IOException, InterruptedException {

		AirlineRecordParser parser = new AirlineRecordParser(value.toString());
		
		if(parser.isHeader()){
			return;
		}
		
		context.write(new IntWritable(Integer.parseInt(parser.getYear())), ONE);
	}

	
	
}
