package com.practice.hadoop.airline.sort;

import org.apache.hadoop.mapreduce.Partitioner;

import com.practice.hadoop.airline.AirlinePartitionerUtil;

public class TotalOrderSortPartitioner extends
		Partitioner<AirlineMonthWeekKeyWritable, AirlineRSWritable> {

	private int indexRange = 83;

	@Override
	public int getPartition(AirlineMonthWeekKeyWritable key,
			AirlineRSWritable value, int numOfReducers) {
		int val = AirlinePartitionerUtil.partitionTotalOrdering(key,
				indexRange, numOfReducers);
		System.out.println("Inside partitioner : key " + key + ", value : "
				+ val + ", index range "+indexRange +", numOfReducer : " + numOfReducers);
		return val;
	}

}
