package com.practice.hadoop.airline.sort;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class SecondarySortComparator extends WritableComparator {

	public SecondarySortComparator() {
		super(AirlineCompositeKey.class, true);
	}

	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		AirlineCompositeKey key1 = (AirlineCompositeKey) a;
		AirlineCompositeKey key2 = (AirlineCompositeKey) b;

		if (key1.getAirportCode().equals(key2.getAirportCode())) {
			Date key1Date = formatDate(key1.getArrivalDate().toString());
			Date key2Date = formatDate(key2.getArrivalDate().toString());
			if (key1Date == null || key2Date == null) {
				return 0;
			}
			return key1Date.compareTo(key2Date);
		} else {
			return key1.getAirportCode().compareTo(key2.getAirportCode());
		}
	}

	public Date formatDate(String date) {
		SimpleDateFormat format = new SimpleDateFormat("MM/DD/YY");
		try {
			return format.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

}
