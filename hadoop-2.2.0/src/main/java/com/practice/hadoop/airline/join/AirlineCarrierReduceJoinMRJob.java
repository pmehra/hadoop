package com.practice.hadoop.airline.join;

import java.io.IOException;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.practice.hadoop.airline.AirlineRecordParser;
import com.practice.hadoop.airline.CarrierRecordParser;
import com.practice.hadoop.airline.join.CarrierKey.CarrierType;

public class AirlineCarrierReduceJoinMRJob extends Configured implements Tool {

	public static class carrierPartitioner extends
			Partitioner<CarrierKey, Text> {

		@Override
		public int getPartition(CarrierKey key, Text value, int numPartitions) {
			return Math.abs(key.getCode().hashCode() % numPartitions);
		}

	}

	public static class CarrerSecondarySort extends WritableComparator {
		public CarrerSecondarySort() {
			super(CarrierKey.class, true);
		}

		@Override
		public int compare(WritableComparable a, WritableComparable b) {
			CarrierKey keyA = (CarrierKey) a;
			CarrierKey keyB = (CarrierKey) b;

			return keyA.compareTo(keyB);
		}

	}

	public static class CarrerGroupSort extends WritableComparator {
		public CarrerGroupSort() {
			super(CarrierKey.class, true);
		}

		@Override
		public int compare(WritableComparable a, WritableComparable b) {
			CarrierKey keyA = (CarrierKey) a;
			CarrierKey keyB = (CarrierKey) b;

			return keyA.getCode().compareTo(keyB.getCode());
		}

	}

	public static class CarrerMasterMapper extends
			Mapper<LongWritable, Text, CarrierKey, Text> {

		private static Text empty = new Text();

		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, CarrierKey, Text>.Context context)
				throws IOException, InterruptedException {
			CarrierRecordParser parser = new CarrierRecordParser(
					value.toString());
			if (parser.isHeader() || !parser.hasCarrierCode()
					|| !parser.hasCarrierDesc()) {
				return;
			}

			CarrierKey carrierKey = new CarrierKey(CarrierType.KEY.value(),
					new Text(parser.getCarrierCode()), new Text(
							parser.getCarrierDesc()));
			System.out.println("Carrier Master Key " + carrierKey);
			context.write(carrierKey, empty);

		}
	}

	public static class FlightDataMapper extends
			Mapper<LongWritable, Text, CarrierKey, Text> {
		private static Text empty = new Text();

		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, CarrierKey, Text>.Context context)
				throws IOException, InterruptedException {
			AirlineRecordParser parser = new AirlineRecordParser(
					value.toString());

			if (parser.isHeader()) {
				return;
			}

			String carrierCode = StringUtils
					.trimToNull(parser.getCarrierCode());
			if (Objects.isNull(carrierCode)
					|| StringUtils.equals(carrierCode, "NA")) {
				return;
			}

			CarrierKey cKey = new CarrierKey(CarrierType.DATA.value(),
					new Text(carrierCode), empty);
			System.out.println("Carrier Flight Key " + cKey);
			context.write(cKey, new Text(parser.selectBasicInfo()));
		}

	}

	public static class AirlineDataJoinReducer extends
			Reducer<CarrierKey, Text, NullWritable, Text> {

		@Override
		protected void reduce(CarrierKey cKey, Iterable<Text> value,
				Reducer<CarrierKey, Text, NullWritable, Text>.Context context)
				throws IOException, InterruptedException {
			String carrierDesc = "UNKNOWN";
			// this is a bug, somehow value type(2) has also desc. This is a
			// mutation problem.
			// because same reference is used sometime these key values are
			// serialized back with incorrect data
			// example in flight data mapper type 2 has never desc being set in
			// but in reduce logs, type 2 has a desc.
			// so somewhere in deserialization and sorting this happened
			System.out.println("Reduce Key " + cKey);
			for (Text text : value) {
				if (cKey.getType().equals(CarrierType.KEY.value())) {
					carrierDesc = cKey.getDesc().toString();
					System.out.println("In key Section " + cKey.getCode() + ","
							+ cKey.getDesc());
				} else {
					System.out.println("In value Section " + cKey.getCode()
							+ "," + cKey.getDesc());
					if (StringUtils.isNotBlank(cKey.getDesc().toString())) {
						carrierDesc = cKey.getDesc().toString();
					}

					context.write(
							NullWritable.get(),
							getValue(text, cKey.getCode().toString(),
									carrierDesc));
				}
			}
		}

		private Text getValue(Text value, String ckey, String desc) {
			return new Text(String.format("(%s,%s)=%s", paturize(ckey),
					paturize(desc), value.toString()));
		}

		public String paturize(String value) {
			return StringUtils.isBlank(value) ? "NA" : value;
		}

	}

	@Override
	public int run(String[] args) throws Exception {
		Job job = Job.getInstance(getConf());
		args = new GenericOptionsParser(args).getRemainingArgs();
		if (args.length < 2) {
			GenericOptionsParser.printGenericCommandUsage(System.err);
			throw new IllegalStateException("path is not set ");
		}
		Path inputCarrier = new Path(args[0]);
		Path inputFilght = new Path(args[1]);
		Path output = new Path(args[2]);

		job.setPartitionerClass(carrierPartitioner.class);
		job.setReducerClass(AirlineDataJoinReducer.class);
		job.setSortComparatorClass(CarrerSecondarySort.class);
		job.setGroupingComparatorClass(CarrerGroupSort.class);

		job.setJarByClass(AirlineCarrierReduceJoinMRJob.class);
		job.setNumReduceTasks(3);
		job.setOutputFormatClass(TextOutputFormat.class);

		job.setMapOutputKeyClass(CarrierKey.class);
		job.setMapOutputValueClass(Text.class);

		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		MultipleInputs.addInputPath(job, inputCarrier, TextInputFormat.class,
				CarrerMasterMapper.class);
		MultipleInputs.addInputPath(job, inputFilght, TextInputFormat.class,
				FlightDataMapper.class);
		FileOutputFormat.setOutputPath(job, output);
		FileSystem.get(getConf()).delete(output, true);

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new AirlineCarrierReduceJoinMRJob(), args));
	}
}
