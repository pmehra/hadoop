package com.practice.hadoop.airline;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class SplitByMonthMRJob extends Configured implements Tool {

	public static class SplitByMonthMapper extends
			Mapper<LongWritable, Text, IntWritable, Text> {

		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, IntWritable, Text>.Context context)
				throws IOException, InterruptedException {
			AirlineRecordParser parser = new AirlineRecordParser(
					value.toString());
			if (parser.isHeader()) {
				return;
			}

			int monthVal = parser.parseInt(parser.getMonth());
			if (monthVal > -1) {
				context.write(new IntWritable(monthVal),
						new Text(parser.getWeekDay()));
			}
		}
	}

	public static class SplitByMonthReducer extends
			Reducer<IntWritable, Text, IntWritable, Text> {

		@Override
		protected void reduce(IntWritable key, Iterable<Text> values,
				Reducer<IntWritable, Text, IntWritable, Text>.Context context)
				throws IOException, InterruptedException {
			for (Text value : values) {
				context.write(key, value);
			}
		}

	}

	public static class SplitByMonthPartitioner extends
			Partitioner<IntWritable, Text> {
		@Override
		public int getPartition(IntWritable key, Text value, int numPartitions) {
			return key.get() - 1;
		}
	}

	@Override
	public int run(String[] args) throws Exception {
		args = new GenericOptionsParser(args).getRemainingArgs();
		if (args.length < 2) {
			ToolRunner.printGenericCommandUsage(System.out);
		}

		Configuration conf = getConf();
		Job job = Job.getInstance(conf);

		Path outputPath = new Path(args[1]);
		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, outputPath);

		job.setJarByClass(SplitByMonthMRJob.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
		job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);

		job.setPartitionerClass(SplitByMonthPartitioner.class);
		job.setNumReduceTasks(12);
		job.setMapperClass(SplitByMonthMapper.class);
		job.setReducerClass(SplitByMonthReducer.class);

		outputPath.getFileSystem(conf).delete(outputPath, true);

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new SplitByMonthMRJob(), args));
	}
}
