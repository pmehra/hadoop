package com.practice.hadoop.airline;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class AggregateByYearAirlineReducer extends
		Reducer<IntWritable, IntWritable, Text, NullWritable> {

	@Override
	protected void reduce(
			IntWritable key,
			Iterable<IntWritable> values,
			Reducer<IntWritable, IntWritable, Text, NullWritable>.Context context)
			throws IOException, InterruptedException {

		int count = 0;
		for (IntWritable intWritable : values) {
			count++;
		}
		context.write(new Text(key.toString() + "-" + count),
				NullWritable.get());
	}

}
