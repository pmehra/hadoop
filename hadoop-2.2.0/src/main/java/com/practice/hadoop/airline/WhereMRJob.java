package com.practice.hadoop.airline;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class WhereMRJob extends Configured implements Tool {

	public static class WhereMapper extends
			Mapper<LongWritable, Text, NullWritable, Text> {

		protected static final String DELAY_THRESHOLD_MIN = "delay.threshold.min";
		long delayThreshold = 0l;

		@Override
		protected void setup(
				Mapper<LongWritable, Text, NullWritable, Text>.Context context)
				throws IOException, InterruptedException {
			delayThreshold = context.getConfiguration().getLong(
					DELAY_THRESHOLD_MIN, 1L);
		}

		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, NullWritable, Text>.Context context)
				throws IOException, InterruptedException {
			AirlineRecordParser record = new AirlineRecordParser(
					value.toString());
			if (record.isHeader()) {
				return;
			}

			long dDelay = record.parseLong(record.getDepartureDelay());
			long aDelay = record.parseLong(record.getArrivalDelay());

			String delayBy = "NA";
			if (dDelay >= delayThreshold && aDelay >= delayThreshold) {
				delayBy = "B";
			} else if (dDelay >= delayThreshold) {
				delayBy = "D";
			} else if (aDelay >= delayThreshold) {
				delayBy = "A";
			}

			String row = record.append(record.selectBasicInfo(), delayBy);
			context.write(NullWritable.get(), new Text(row));
		}
	}

	@Override
	public int run(String[] args) throws Exception {

		args = new GenericOptionsParser(args).getRemainingArgs();
		if (args.length < 2) {
			ToolRunner.printGenericCommandUsage(System.err);
		}

		Configuration conf = getConf();
		Job job = Job.getInstance(conf);
		job.setJarByClass(WhereMRJob.class);
		job.setNumReduceTasks(0);

		job.setMapperClass(WhereMapper.class);
		job.setInputFormatClass(TextInputFormat.class);

		job.setOutputFormatClass(TextOutputFormat.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		FileSystem.get(conf).delete(new Path(args[1]), true);

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new WhereMRJob(), args));
	}

}
