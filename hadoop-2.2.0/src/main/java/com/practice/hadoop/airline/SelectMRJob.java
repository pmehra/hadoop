package com.practice.hadoop.airline;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class SelectMRJob extends Configured implements Tool {

	public static class SelectMap extends
			Mapper<LongWritable, Text, NullWritable, Text> {

		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, NullWritable, Text>.Context context)
				throws IOException, InterruptedException {
			AirlineRecordParser parser = new AirlineRecordParser(
					value.toString());
			if (!parser.isHeader()) {
				context.write(NullWritable.get(),
						new Text(parser.selectBasicInfo()));
			}
		}

	}

	@Override
	public int run(String[] args) throws Exception {

		args = new GenericOptionsParser(args).getRemainingArgs();
		if (args.length < 2) {
			ToolRunner.printGenericCommandUsage(System.err);
		}

		Configuration conf = getConf();
		Job job = Job.getInstance(conf);

		Path outputPath = new Path(args[1]);
		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, outputPath);

		job.setJarByClass(SelectMRJob.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);
		job.setNumReduceTasks(0);
		job.setMapperClass(SelectMap.class);

		outputPath.getFileSystem(conf).delete(outputPath, true);

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new SelectMRJob(), args));
	}

}
