package com.practice.hadoop.airline.sort;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.practice.hadoop.airline.AirlineRecordParser;

public class TotalOrderSortingMRJob extends Configured implements Tool {

	public static class TotalOrderSortMapper
			extends
			Mapper<LongWritable, Text, AirlineMonthWeekKeyWritable, AirlineRSWritable> {

		@Override
		protected void map(
				LongWritable key,
				Text value,
				Mapper<LongWritable, Text, AirlineMonthWeekKeyWritable, AirlineRSWritable>.Context context)
				throws IOException, InterruptedException {

			AirlineRecordParser parser = new AirlineRecordParser(
					value.toString());
			if (parser.isHeader()) {
				return;
			}

			int month = parser.parseInt(parser.getMonth());
			int dayOfWeek = parser.getWeekDayAsInt();
			if (month < 0 || dayOfWeek < 0) {
				return;
			}
			
			System.out.println(month +","+dayOfWeek);
			AirlineMonthWeekKeyWritable comparablekey = new AirlineMonthWeekKeyWritable();
			comparablekey.setMonth(new IntWritable(month));
			comparablekey.setDayOfWeek(new IntWritable(dayOfWeek));

			AirlineRSWritable airlineRSWritable = new AirlineRSWritable(parser);
			context.write(comparablekey, airlineRSWritable);

		}

	}

	public static class TotalOrderSortReducer
			extends
			Reducer<AirlineMonthWeekKeyWritable, AirlineRSWritable, NullWritable, Text> {

		@Override
		protected void reduce(
				AirlineMonthWeekKeyWritable arg0,
				Iterable<AirlineRSWritable> arg1,
				Reducer<AirlineMonthWeekKeyWritable, AirlineRSWritable, NullWritable, Text>.Context context)
				throws IOException, InterruptedException {
			System.out.println("key "+arg0);
			for (AirlineRSWritable airlineRSWritable : arg1) {
				context.write(NullWritable.get(), getVal(airlineRSWritable));
			}
		}

		private Text getVal(AirlineRSWritable value) {
			StringBuilder sb = new StringBuilder();
			sb.append(value.getDate() + "--");
			sb.append(value.getMonth() + "," + value.getDayOfWeek() + "");
			return new Text(sb.toString());

		}

	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new TotalOrderSortingMRJob(), args));
	}

	@Override
	public int run(String[] args) throws Exception {

		args = new GenericOptionsParser(args).getRemainingArgs();
		if (args.length < 2) {
			GenericOptionsParser.printGenericCommandUsage(System.err);
		}

		Configuration conf = getConf();
		Job job = Job.getInstance(conf);

		job.setJarByClass(TotalOrderSortingMRJob.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setMapOutputKeyClass(AirlineMonthWeekKeyWritable.class);
		job.setMapOutputValueClass(AirlineRSWritable.class);

		job.setMapperClass(TotalOrderSortMapper.class);
		job.setReducerClass(TotalOrderSortReducer.class);
		job.setPartitionerClass(TotalOrderSortPartitioner.class);
		job.setNumReduceTasks(12);

		FileSystem.get(conf).delete(new Path(args[1]), true);
		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		return job.waitForCompletion(true) ? 0 : 1;
	}

}
