package com.practice.hadoop.airline;

import com.practice.hadoop.airline.sort.AirlineMonthWeekKeyWritable;

public class AirlinePartitionerUtil {

	/**
	 * - Get range per reducer
	 * - Get unique index
	 * - if the reducers are greater than the key range that means all the reducer gets 1 element each and if
	 *   reducers are more than the key range the after exhausting range lftover will get 0 elements
	 * - Iterate all reducers, caluclate their range (start,end). See if unique index lies in tha range.
	 * - if they reach last step that means key range is less than the num of redcuer, so each reducer gets one 
	 *   element.   
	 */
	public static int partitionTotalOrdering(AirlineMonthWeekKeyWritable key, int keyRange, int numOfReducers) {
		int rangePerReducer = (int) Math.floor(keyRange / numOfReducers);
		System.out.println("rangePerReducer" + rangePerReducer);
		int uniqueIndex = (key.getMonth().get() - 1) * 7
				+ (7 - key.getDayOfWeek().get());
		System.out.println("uniqueIndex" + uniqueIndex);
		if (keyRange < numOfReducers) {
			return uniqueIndex;
		}
		
		for (int i = 0; i < numOfReducers; i++) {
			int inclusiveRange = i * rangePerReducer;
			int exclusiveRange = (i + 1) * rangePerReducer;
			
			if(uniqueIndex >= inclusiveRange  &&  uniqueIndex< exclusiveRange){
				System.out.println("inclusiveRange,exclusiveRange" + inclusiveRange+","+exclusiveRange+","+uniqueIndex);
				return i;
			}
		}
		int val = (numOfReducers -1);
		System.out.println("val" + val);
		return val;
	}
}
