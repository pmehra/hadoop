package com.practice.hadoop.airline;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class AggregateMRJob extends Configured implements Tool {

	private static IntWritable record = new IntWritable(0);
	private static IntWritable arrivedOnTime = new IntWritable(1);
	private static IntWritable arrivedDelay = new IntWritable(2);
	private static IntWritable departOnTime = new IntWritable(3);
	private static IntWritable departDelay = new IntWritable(4);
	private static IntWritable isDiverted = new IntWritable(5);
	private static IntWritable isCancelled = new IntWritable(6);

	public static class AggregateMapper extends
			Mapper<LongWritable, Text, Text, IntWritable> {

		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, Text, IntWritable>.Context context)
				throws IOException, InterruptedException {

			AirlineRecordParser parser = new AirlineRecordParser(
					value.toString());

			if (parser.isHeader()) {
				return;
			}

			Text month = new Text(parser.getMonthAsText());

			context.write(month, record);

			if (parser.parseInt(parser.getArrivalDelay()) > 0) {
				context.write(month, arrivedDelay);
			} else {
				context.write(month, arrivedOnTime);
			}

			if (parser.parseInt(parser.getDepartureDelay()) > 0) {
				context.write(month, departDelay);
			} else {
				context.write(month, departOnTime);
			}

			if (parser.isCancelled()) {
				context.write(month, isCancelled);
			}

			if (parser.isDiverted()) {
				context.write(month, isDiverted);
			}
		}
	}

	public static class AggregateReducer extends
			Reducer<Text, IntWritable, Text, Text> {

		private static String OPUT_FORMAT = "%S(month), %s(totalFlights),"
				+ " %s(arrivedOnTime), %s(arrivedDelay),"
				+ " %s(departOnTime), %s(departDelay),"
				+ " %s(cancelled), %s(diverted)";

		@Override
		protected void reduce(Text key, Iterable<IntWritable> values,
				Reducer<Text, IntWritable, Text, Text>.Context context)
				throws IOException, InterruptedException {

			int recordCount = 0;
			int arrivedOnTimeCount = 0;
			int arrivedDelayCount = 0;
			int departOnTimeCount = 0;
			int departDelayCount = 0;
			int cancelledCount = 0;
			int divertedCount = 0;

			for (IntWritable value : values) {
				final int valueAsInt = value.get();

				recordCount = incrementCount(valueAsInt, record.get(),
						recordCount);

				arrivedOnTimeCount = incrementCount(valueAsInt,
						arrivedOnTime.get(), arrivedOnTimeCount);

				arrivedDelayCount = incrementCount(valueAsInt,
						arrivedDelay.get(), arrivedDelayCount);

				departOnTimeCount = incrementCount(valueAsInt,
						departOnTime.get(), departOnTimeCount);

				departDelayCount = incrementCount(valueAsInt,
						departDelay.get(), departDelayCount);

				cancelledCount = incrementCount(valueAsInt, isCancelled.get(),
						cancelledCount);

				divertedCount = incrementCount(valueAsInt, isDiverted.get(),
						divertedCount);
			}

			String output = String.format(OPUT_FORMAT, key.toString(),
					recordCount, arrivedOnTimeCount, arrivedDelayCount,
					departOnTimeCount, departDelayCount, cancelledCount,
					divertedCount);

			context.write(key, new Text(output));
		}

		private int incrementCount(int value, int toBeComparedvalue, int count) {
			if (value == toBeComparedvalue) {
				return count + 1;
			}
			return count;
		}
	}

	@Override
	public int run(String[] args) throws Exception {

		args = new GenericOptionsParser(args).getRemainingArgs();
		if (args.length < 2) {
			ToolRunner.printGenericCommandUsage(System.err);
		}

		Configuration conf = getConf();
		Job job = Job.getInstance(conf);

		Path outputPath = new Path(args[1]);
		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, outputPath);

		job.setJarByClass(AggregateMRJob.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setNumReduceTasks(1);
		job.setMapperClass(AggregateMapper.class);
		job.setReducerClass(AggregateReducer.class);

		outputPath.getFileSystem(conf).delete(outputPath, true);

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new AggregateMRJob(), args));
	}

}
