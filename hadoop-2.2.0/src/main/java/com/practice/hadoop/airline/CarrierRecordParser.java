package com.practice.hadoop.airline;

import org.apache.commons.lang.StringUtils;

public class CarrierRecordParser {

	private static final String NA = "NA";
	private String[] arr;

	public CarrierRecordParser(String line) {
		if (StringUtils.isBlank(line)) {
			arr = new String[] { null, null };
		} else {
			arr = line.split(",");
		}
	}

	public boolean isHeader() {
		return !get(0).equals(NA) && get(0).equals("Code");
	}

	public boolean hasCarrierCode() {
		return !get(0).equals(NA);
	}

	public boolean hasCarrierDesc() {
		return !get(1).equals(NA);
	}

	public String getCarrierCode() {
		return StringUtils.remove(get(0), '"');
	}

	public String getCarrierDesc() {
		return StringUtils.remove(get(1), '"');
	}

	private String get(int index) {
		if (arr.length - 1 < index) {
			return NA;
		}
		return StringUtils.defaultIfEmpty(arr[index], NA);
	}

}
