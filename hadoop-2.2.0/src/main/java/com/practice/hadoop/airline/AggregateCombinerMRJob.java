package com.practice.hadoop.airline;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class AggregateCombinerMRJob extends Configured implements Tool {

	private static IntWritable record = new IntWritable(0);
	private static IntWritable arrivedOnTime = new IntWritable(1);
	private static IntWritable arrivedDelay = new IntWritable(2);
	private static IntWritable departOnTime = new IntWritable(3);
	private static IntWritable departDelay = new IntWritable(4);
	private static IntWritable isDiverted = new IntWritable(5);
	private static IntWritable isCancelled = new IntWritable(6);
	private static IntWritable ONE = new IntWritable(1);

	public static class AggregateMapper extends
			Mapper<LongWritable, Text, Text, MapWritable> {

		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, Text, MapWritable>.Context context)
				throws IOException, InterruptedException {

			AirlineRecordParser parser = new AirlineRecordParser(
					value.toString());

			if (parser.isHeader()) {
				return;
			}

			Text month = new Text(parser.getMonthAsText());

			context.write(month, getMap(record, ONE));

			if (parser.parseInt(parser.getArrivalDelay()) > 0) {
				context.write(month, getMap(arrivedDelay, ONE));
			} else {
				context.write(month, getMap(arrivedOnTime, ONE));
			}

			if (parser.parseInt(parser.getDepartureDelay()) > 0) {
				context.write(month, getMap(departDelay, ONE));
			} else {
				context.write(month, getMap(departOnTime, ONE));
			}

			if (parser.isCancelled()) {
				context.write(month, getMap(isCancelled, ONE));
			}

			if (parser.isDiverted()) {
				context.write(month, getMap(isDiverted, ONE));
			}
		}

	}

	public static class AggregateCombiner extends
			Reducer<Text, MapWritable, Text, MapWritable> {

		@Override
		protected void reduce(Text key, Iterable<MapWritable> values,
				Context context) throws IOException, InterruptedException {

			int recordCount = 0;
			int arrivedOnTimeCount = 0;
			int arrivedDelayCount = 0;
			int departOnTimeCount = 0;
			int departDelayCount = 0;
			int cancelledCount = 0;
			int divertedCount = 0;

			for (MapWritable mapWritable : values) {

				Set<Entry<Writable, Writable>> entrySet = mapWritable
						.entrySet();
				for (Entry<Writable, Writable> entry : entrySet) {
					IntWritable type = (IntWritable) entry.getKey();
					IntWritable value = (IntWritable) entry.getValue();

					recordCount = incrementCount(type.get(), record.get(),
							value.get(), recordCount);

					arrivedOnTimeCount = incrementCount(type.get(),
							arrivedOnTime.get(), value.get(),
							arrivedOnTimeCount);

					arrivedDelayCount = incrementCount(type.get(),
							arrivedDelay.get(), value.get(), arrivedDelayCount);

					departOnTimeCount = incrementCount(type.get(),
							departOnTime.get(), value.get(), departOnTimeCount);

					departDelayCount = incrementCount(type.get(),
							departDelay.get(), value.get(), departDelayCount);

					cancelledCount = incrementCount(type.get(),
							isCancelled.get(), value.get(), cancelledCount);

					divertedCount = incrementCount(type.get(),
							isDiverted.get(), value.get(), divertedCount);
				}
			}

			context.write(key, getMap(record, new IntWritable(recordCount)));
			context.write(key,
					getMap(arrivedOnTime, new IntWritable(arrivedOnTimeCount)));
			context.write(key,
					getMap(arrivedDelay, new IntWritable(arrivedDelayCount)));
			context.write(key,
					getMap(departOnTime, new IntWritable(departOnTimeCount)));
			context.write(key,
					getMap(departDelay, new IntWritable(departDelayCount)));
			context.write(key,
					getMap(isCancelled, new IntWritable(cancelledCount)));
			context.write(key,
					getMap(isDiverted, new IntWritable(divertedCount)));
		}
	}

	public static class AggregateReducer extends
			Reducer<Text, MapWritable, Text, Text> {

		private static String OPUT_FORMAT = "%S(month), %s(totalFlights),"
				+ " %s(arrivedOnTime), %s(arrivedDelay),"
				+ " %s(departOnTime), %s(departDelay),"
				+ " %s(cancelled), %s(diverted)";

		@Override
		protected void reduce(Text key, Iterable<MapWritable> values,
				Context context) throws IOException, InterruptedException {

			int recordCount = 0;
			int arrivedOnTimeCount = 0;
			int arrivedDelayCount = 0;
			int departOnTimeCount = 0;
			int departDelayCount = 0;
			int cancelledCount = 0;
			int divertedCount = 0;

			for (MapWritable mapWritable : values) {

				Set<Entry<Writable, Writable>> entrySet = mapWritable
						.entrySet();
				for (Entry<Writable, Writable> entry : entrySet) {
					IntWritable type = (IntWritable) entry.getKey();
					IntWritable value = (IntWritable) entry.getValue();

					recordCount = incrementCount(type.get(), record.get(),
							value.get(), recordCount);

					arrivedOnTimeCount = incrementCount(type.get(),
							arrivedOnTime.get(), value.get(),
							arrivedOnTimeCount);

					arrivedDelayCount = incrementCount(type.get(),
							arrivedDelay.get(), value.get(), arrivedDelayCount);

					departOnTimeCount = incrementCount(type.get(),
							departOnTime.get(), value.get(), departOnTimeCount);

					departDelayCount = incrementCount(type.get(),
							departDelay.get(), value.get(), departDelayCount);

					cancelledCount = incrementCount(type.get(),
							isCancelled.get(), value.get(), cancelledCount);

					divertedCount = incrementCount(type.get(),
							isDiverted.get(), value.get(), divertedCount);
				}
			}

			String output = String.format(OPUT_FORMAT, key.toString(),
					recordCount, arrivedOnTimeCount, arrivedDelayCount,
					departOnTimeCount, departDelayCount, cancelledCount,
					divertedCount);

			context.write(key, new Text(output));
		}

	}

	public static class MonthPartiotioner extends
			Partitioner<Text, MapWritable> {

		private static Map<String, Integer> monthMap = new HashMap<String, Integer>();
		static {
			int i = 0;
			for (String month : AirlineRecordParser.months) {
				monthMap.put(month, i++);
			}
		}

		@Override
		public int getPartition(Text key, MapWritable value, int numPartitions) {
			return monthMap.get(key.toString());
		}

	}

	private static int incrementCount(int value, int toBeComparedvalue,
			int existingCount, int count) {
		if (value == toBeComparedvalue) {
			return count + existingCount;
		}
		return count;
	}

	private static MapWritable getMap(IntWritable type, IntWritable value) {
		MapWritable map = new MapWritable();
		map.put(type, value);
		return map;
	}

	@Override
	public int run(String[] args) throws Exception {

		args = new GenericOptionsParser(args).getRemainingArgs();
		if (args.length < 2) {
			ToolRunner.printGenericCommandUsage(System.err);
		}

		Configuration conf = getConf();
		Job job = Job.getInstance(conf);

		Path outputPath = new Path(args[1]);
		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, outputPath);

		job.setJarByClass(AggregateCombinerMRJob.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(MapWritable.class);

		job.setNumReduceTasks(2);
		job.setMapperClass(AggregateMapper.class);
		job.setReducerClass(AggregateReducer.class);
		job.setCombinerClass(AggregateCombiner.class);
		job.setPartitionerClass(MonthPartiotioner.class);

		outputPath.getFileSystem(conf).delete(outputPath, true);

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new AggregateCombinerMRJob(), args));
	}

}
