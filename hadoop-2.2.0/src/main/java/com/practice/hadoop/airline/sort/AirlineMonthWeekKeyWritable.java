package com.practice.hadoop.airline.sort;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;

public class AirlineMonthWeekKeyWritable implements
		WritableComparable<AirlineMonthWeekKeyWritable> {

	private IntWritable month = new IntWritable();
	private IntWritable dayOfWeek = new IntWritable();

	@Override
	public void write(DataOutput out) throws IOException {
		month.write(out);
		dayOfWeek.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		month.readFields(in);
		dayOfWeek.readFields(in);
	}

	@Override
	public int compareTo(AirlineMonthWeekKeyWritable obj) {
		if (this.month.get() == obj.month.get()) {
			return -1 * this.dayOfWeek.compareTo(obj.dayOfWeek);
		} else {
			return 1 * this.dayOfWeek.compareTo(obj.dayOfWeek);
		}
	}

	@Override
	public int hashCode() {
		return (this.month.get() - 1) * 7;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AirlineMonthWeekKeyWritable other = (AirlineMonthWeekKeyWritable) obj;
		if (dayOfWeek == null) {
			if (other.dayOfWeek != null)
				return false;
		} else if (!(dayOfWeek.get() == other.dayOfWeek.get()))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!(month.get() == other.month.get()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[month=" + month + ", dayOfWeek="
				+ dayOfWeek + "]";
	}

	public IntWritable getMonth() {
		return month;
	}

	public void setMonth(IntWritable month) {
		this.month = month;
	}

	public IntWritable getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(IntWritable dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

}
