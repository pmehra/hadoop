package com.practice.hadoop.airline.sort;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class GroupSortComparator extends WritableComparator {

	public GroupSortComparator() {
		super(AirlineCompositeKey.class, true);
	}

	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		/*
		 * AirlineCompositeKey key1 = (AirlineCompositeKey) a;
			AirlineCompositeKey key2 = (AirlineCompositeKey) b;
		key1.getAirportCode().compareTo(key2.getAirportCode());
		
		Could have used this but using AirlineCompositeKey compareTo method
		 * */
		return a.compareTo(b);
	}

}
