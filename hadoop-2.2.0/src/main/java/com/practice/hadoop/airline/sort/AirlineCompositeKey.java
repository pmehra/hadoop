package com.practice.hadoop.airline.sort;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class AirlineCompositeKey implements
		WritableComparable<AirlineCompositeKey> {

	private Text airportCode = new Text();
	private Text arrivalDate = new Text();

	public AirlineCompositeKey() {
	}

	public AirlineCompositeKey(Text airportCode, Text arrivalDate) {
		this.airportCode = airportCode;
		this.arrivalDate = arrivalDate;
	}

	public Text getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(Text airportCode) {
		this.airportCode = airportCode;
	}

	public Text getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Text arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		airportCode.write(out);
		arrivalDate.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		airportCode.readFields(in);
		arrivalDate.readFields(in);
	}

	@Override
	public int compareTo(AirlineCompositeKey o) {
		return this.airportCode.compareTo(o.airportCode);
	}

	@Override
	public int hashCode() {
		return airportCode.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AirlineCompositeKey other = (AirlineCompositeKey) obj;
		if (airportCode == null) {
			if (other.airportCode != null)
				return false;
		} else if (!airportCode.equals(other.airportCode))
			return false;
		return true;
	}

}
