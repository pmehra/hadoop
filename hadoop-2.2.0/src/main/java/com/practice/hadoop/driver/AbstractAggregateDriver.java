package com.practice.hadoop.driver;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.practice.hadoop.airline.AggregateByYearAirlineMapper;
import com.practice.hadoop.airline.AggregateByYearAirlineReducer;

public abstract class AbstractAggregateDriver extends Configured implements
		Tool {

	@Override
	public int run(String[] args) throws Exception {

		args = new GenericOptionsParser(args).getRemainingArgs();
		if (args.length < 2) {
			ToolRunner.printGenericCommandUsage(System.err);
		}

		Configuration conf = getConf();
		Job job = Job.getInstance(conf);
		job.setJarByClass(getClassType());
		Path outputDir = new Path(args[1]);
		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, outputDir);

		setInputOutPutFormats(job);
		setConfigurationParams(job,conf);

		job.setMapperClass(AggregateByYearAirlineMapper.class);
		job.setReducerClass(AggregateByYearAirlineReducer.class);
		job.setNumReduceTasks(1);
		setOutputKeyValues(job);

		FileSystem.get(conf).delete(outputDir, true);
		return job.waitForCompletion(true) ? 0 : 1;
	}

	protected void setConfigurationParams(Job job,Configuration conf) {
		// hook
	}

	protected void setOutputKeyValues(Job job) {
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(IntWritable.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(NullWritable.class);
	}

	protected void setInputOutPutFormats(Job job) {
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
	}

	protected abstract Class<?> getClassType();

}
