package com.practice.hadoop.driver;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.ToolRunner;

public class ReadCompressedAggregateDriver extends AbstractAggregateDriver {

	@Override
	protected void setConfigurationParams(Job job, Configuration conf) {
		FileOutputFormat.setCompressOutput(job, true);
		FileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

	}

	@Override
	protected Class<?> getClassType() {
		return this.getClass();
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new ReadCompressedAggregateDriver(), args));
	}
}
