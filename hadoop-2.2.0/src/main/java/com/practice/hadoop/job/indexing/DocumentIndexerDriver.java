package com.practice.hadoop.job.indexing;

import java.util.Random;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class DocumentIndexerDriver extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		Job job = Job.getInstance(getConf());
		job.setNumReduceTasks(4);
		job.setJarByClass(DocumentIndexerDriver.class);
		job.setMapperClass(DocumentIndexerMapper.class);
		job.setPartitionerClass(DocumentIndexerPartitioner.class);
		job.setReducerClass(DocumentIndexerReducer.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);

		Path outputPath = new Path(args[1]);
		FileInputFormat.setInputPaths(job, args[0]);
		FileOutputFormat.setOutputPath(job, outputPath);

		outputPath.getFileSystem(getConf()).delete(outputPath, true);

		return job.waitForCompletion(true) ? 0 : 1;
	}
	
	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new DocumentIndexerDriver(), args));
	}

}
