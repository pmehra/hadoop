package com.practice.hadoop.job.indexing;

import java.io.IOException;
import java.util.StringJoiner;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class DocumentIndexerReducer extends Reducer<Text, Text, Text, Text> {

	@Override
	protected void reduce(Text word, Iterable<Text> documentIDs, Context context)
			throws IOException, InterruptedException {
		StringJoiner joiner = new StringJoiner(",");
		for (Text document : documentIDs) {
			joiner.add(document.toString());
		}

		context.write(word, new Text(joiner.toString()));
	}

}
