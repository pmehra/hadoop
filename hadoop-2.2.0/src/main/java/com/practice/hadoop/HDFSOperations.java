package com.practice.hadoop;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.util.Progressable;

/**
 * The Class HDFSOperations.
 */
public class HDFSOperations {

    /**
     * Copy from local.
     *
     * @param targetConfig the target config
     * @param inLocal the in local
     * @param outHdfs the out hdfs
     * @param fileNameInHdfs the file name in hdfs
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void copyFromLocal(Configuration targetConfig, String inLocal, String outHdfs,
            String fileNameInHdfs) throws IOException {

        if (targetConfig == null || inLocal == null || outHdfs == null) {
            throw new IllegalArgumentException("CopyFromLocal input cannot be null");
        }

        InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(inLocal);
        if (in == null) {
            throw new IllegalArgumentException("Local file does not exist " + inLocal);
        }
        in = new BufferedInputStream(in);

        FileSystem hdfs = null;
        try {
            hdfs = FileSystem.get(targetConfig);
            String fullOut = outHdfs + "/" + fileNameInHdfs;

            Path outHdfsPath = new Path(fullOut);
            if (hdfs.exists(outHdfsPath)) {
                throw new IllegalArgumentException("File already present in HDFS "
                        + outHdfsPath.toString());
            }

            FSDataOutputStream out = hdfs.create(outHdfsPath, new Progressable() {

                @Override
                public void progress() {
                    System.out.print(".");
                }
            });
            IOUtils.copyBytes(in, out, 4096, true);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            IOUtils.closeStream(in);
        }
    }

    /**
     * Delete file.
     *
     * @param targetConfig the target config
     * @param location the location
     * @return true, if successful
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static boolean deleteFile(Configuration targetConfig, String location)
            throws IOException {
        if (location == null || location.trim().isEmpty()) {
            throw new IllegalArgumentException("location is null/empty");
        }
        FileSystem fs = FileSystem.get(URI.create(location), targetConfig);
        return fs.delete(new Path(location), true);
    }
}
