package com.practice.hadoop;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.io.compress.CompressionOutputStream;

public class CommonOperations {

	private static final String tmpEnv = "java.io.tmpdir";

	public static String getTempPath() {
		return System.getProperty(tmpEnv);
	}

	public static String getFindableTempPath(String prefix) {
		return getTempPath() + "/" + prefix + "-"
				+ UUID.randomUUID().toString();
	}

	/**
	 * if the file is under some folder then use folder/filename e.g.
	 * ncdc/input.txt
	 * 
	 * @param fileName
	 * @return absolute path
	 */
	public static String getClassPathFileAbsPath(String fileName) {
		return ClassLoader.getSystemClassLoader().getResource(fileName)
				.getPath();
	}

	public static String compressFile(CodecType codec, String filePath,
			Configuration conf) {
		if (conf == null) {
			conf = new Configuration();
		}
		FileSystem fs = null;
		try {
			fs = FileSystem.get(URI.create(filePath), conf);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		Path input = new Path(filePath);
		String outputPath = removeExtension(filePath) + "Compress."
				+ codec.getValue();

		CompressionCodecFactory factory = new CompressionCodecFactory(conf);
		CompressionCodec compressionCodec = factory.getCodecByName(codec
				.getValue());

		InputStream in = null;
		OutputStream out = null;
		CompressionOutputStream codecOut = null;
		try {
			in = fs.open(input);
			out = fs.create(new Path(outputPath));
			codecOut = compressionCodec.createOutputStream(out);
			IOUtils.copyBytes(in, codecOut, conf);
			codecOut.finish();
		} catch (IOException e) {

			throw new IllegalStateException(e);
		} finally {
			IOUtils.closeStream(in);
			IOUtils.closeStream(out);
			IOUtils.closeStream(codecOut);
		}

		return outputPath;
	}

	private static String removeExtension(String filePath) {
		String outputPath = filePath;
		int dotIndex = filePath.lastIndexOf('.');
		if (dotIndex > 0) {
			outputPath = filePath.substring(0, dotIndex);
		}
		return outputPath;
	}

	public static FileSystem getLocalhostFS(String uri) {
		Configuration conf = new Configuration();
		conf.addResource("hadoop-localhost.xml");
		try {
			return FileSystem.get(URI.create(uri), conf);
		} catch (IOException e) {
			throw new IllegalStateException("cant get filesystem object", e);
		}
	}

	public static FileSystem getLocalFS(String uri) {
		Configuration conf = getLocalConfiguration();
		try {
			return FileSystem.get(URI.create(uri), conf);
		} catch (IOException e) {
			throw new IllegalStateException("cant get local filesystem object",
					e);
		}
	}

	public static Configuration getLocalConfiguration() {
		Configuration conf = new Configuration();
		conf.addResource("hadoop-local.xml");
		return conf;
	}

	public static Admin getHBaseLocal() {
		Configuration conf = new Configuration();
		conf.addResource("hbase-local.xml");
		try {
			return ConnectionFactory.createConnection(conf).getAdmin();
		} catch (IOException e) {
			throw new IllegalStateException(
					"cant get local filesystem fo hbase", e);
		}

	}

}
