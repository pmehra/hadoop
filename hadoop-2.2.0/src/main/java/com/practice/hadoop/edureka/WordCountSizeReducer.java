package com.practice.hadoop.edureka;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WordCountSizeReducer extends
		Reducer<IntWritable, Text, IntWritable, Text> {

	protected static final String verbiage = "%s (As the word of size %s %s : %s)";

	@Override
	protected void reduce(IntWritable key, Iterable<Text> values,
			Reducer<IntWritable, Text, IntWritable, Text>.Context context)
			throws IOException, InterruptedException {

		String strValue = "";
		Iterator<Text> itr = values.iterator();
		int i = 0;
		for (; itr.hasNext(); i++) {
			if (i > 0) {
				strValue += ",";
			}
			strValue += itr.next().toString();
		}

		context.write(key, new Text(formatedVerbiage(key.get(), strValue, i)));

	}

	protected static String formatedVerbiage(int key, String strValue,
			int i) {
		return String.format(verbiage, i, key, (i > 1) ?  "are" : "is",
				strValue);
	}

}
