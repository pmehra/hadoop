package com.practice.hadoop.edureka;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class WordCountSizeDriver extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		if (args == null || args.length < 2) {
			System.err.println("Usage: WordCountSize <input> <output>");
			ToolRunner.printGenericCommandUsage(System.err);
		}

		Job job = Job.getInstance(getConf(), "WordCountSize");
		job.setJarByClass(WordCountSizeDriver.class);

		job.setMapperClass(WordCountSizeMapper.class);
		job.setReducerClass(WordCountSizeReducer.class);

		job.setInputFormatClass(TextInputFormat.class);
		FileInputFormat.setInputPaths(job, new Path(args[0]));

		job.setOutputFormatClass(TextOutputFormat.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new WordCountSizeDriver(), args);
		System.exit(exitCode);
	}

}
