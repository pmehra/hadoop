package com.practice.hadoop.edureka;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WordCountSizeMapper extends
		Mapper<LongWritable, Text, IntWritable, Text> {

	private static final IntWritable ZERO = new IntWritable(0);
	private static final Text EMPTY_TEXT = new Text("");

	enum InvalidToken {
		BLANK_TOKENS;
	}

	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, IntWritable, Text>.Context context)
			throws IOException, InterruptedException {
		StringTokenizer tokenized = new StringTokenizer(value.toString());
		while (tokenized.hasMoreTokens()) {
			String token = tokenized.nextToken();
			if (token == null || token.isEmpty()) {
				context.getCounter(InvalidToken.BLANK_TOKENS).increment(1);
				context.write(ZERO, EMPTY_TEXT);
				return;
			}

			context.write(new IntWritable(token.length()), new Text(token));
		}

	}

}
