package com.practice.avro;

import java.io.IOException;
import java.io.InputStream;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.practice.hadoop.airline.AirlineRecordParser;

public class AirlineDataAvroFormatterMRJob extends Configured implements Tool {

	public static Schema schema;

	static {
		try {
			InputStream in = AirlineDataAvroFormatterMRJob.class
					.getClassLoader().getResourceAsStream(
							"avrofiles/airline.avsc");
			schema = new Schema.Parser().parse(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static class AirlineAvroFormatterMapper extends
			Mapper<LongWritable, Text, AvroKey<GenericRecord>, NullWritable> {

		GenericRecord gr = new GenericData.Record(schema);

		@Override
		protected void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			AirlineRecordParser parser = new AirlineRecordParser(
					value.toString());
			if (parser.isHeader()) {
				return;
			}

			gr.put("Date", parser.getFlightDate());
			gr.put("Origin", parser.getOrigin());
			gr.put("Dest", parser.getDestination());

			context.write(new AvroKey<GenericRecord>(gr), NullWritable.get());
		}

	}

	@Override
	public int run(String[] args) throws Exception {
		args = new GenericOptionsParser(args).getRemainingArgs();
		if (args.length < 2) {
			ToolRunner.printGenericCommandUsage(System.out);
			return -1;
		}

		Configuration conf = getConf();
		conf.set(Job.MAPREDUCE_JOB_USER_CLASSPATH_FIRST, "true");
		Job job = Job.getInstance(conf);
		job.setJarByClass(AirlineDataAvroFormatterMRJob.class);

		Path outputDir = new Path(args[1]);

		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, outputDir);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(AvroKeyOutputFormat.class);

		job.setMapOutputKeyClass(AvroKey.class);
		job.setMapOutputValueClass(NullWritable.class);
		AvroJob.setMapOutputKeySchema(job, schema);

		job.setNumReduceTasks(0);
		job.setMapperClass(AirlineAvroFormatterMapper.class);

		outputDir.getFileSystem(conf).delete(outputDir, true);

		return job.waitForCompletion(true) ? 0 : 1;

	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new AirlineDataAvroFormatterMRJob(), args));
	}
}
