/*
 * Script to calculate 
 * Customer's profession with highest spending.
 *
 * pig -x local -m mypig.param customer.pig
 *
 */
 
 IMPORT './common.macro';
 
 --Count customer by profession
 
 cust = LOAD '/home/pmehra/learning/hadoop/code/hadoop/input/customer/custs' USING PigStorage(',') AS (custId:long, fname:chararray, lname:chararray, age:int, profession:chararray);
 grp_cust_by_profession = GROUP cust BY profession;
 counted_customer = FOREACH grp_cust_by_profession GENERATE group, COUNT(cust);
 
 -- load trxns
 trxns = LOAD '/home/pmehra/learning/hadoop/code/hadoop/input/customer/txns-sample.txt' USING PigStorage(',') AS (txnId:long, txndate:chararray, custId:long, amt:double, category:chararray, subcat:chararray, city:chararray, state:chararray, txntype:chararray);
 	 
grp_trxns = GROUP trxns BY custId;
custId_amt = FOREACH grp_trxns GENERATE group, SUM(trxns.amt) AS total;

-- here join result will be custId,total  ,   custId,fname,lname,age, profession
--                          ============      ==================================  
join_cust = JOIN custId_amt by $0, cust by $0;

join_cust_limit = LIMIT join_cust 10;
display_cust = FOREACH join_cust_limit GENERATE $0 /* CustId*/, CONCAT($3,$4) /*fname+lname*/, $6 /*profession*/, $1 /*total Amt */;
DUMP display_cust;

 	 
 
 
 