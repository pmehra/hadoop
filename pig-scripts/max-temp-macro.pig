


 IMPORT './common.macro';


records = LOAD '/home/pmehra/data/ncdc/micro-tab/sample.txt' AS (year:chararray, temp:int, quality:int );

filtered_records = FILTER records BY temp != 9999 AND quality == 1;

group_by_year_records = GROUP filtered_records BY year;

dump group_by_year_records;

--see filtered_records

--max_temp = max_by_group(filtered_records,year, temp);

--dump max_temp

