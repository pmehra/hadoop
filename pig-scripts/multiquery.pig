
/*
 * if use via run(interactive mode), then PIG will run STORE twice
 * If use via exec (batch mode), then PIG will improviose and run STORE once.
 *
 * '/home/pmehra/data/pig/multiquery'
 *  IMPORT './common.macro';
 */
  
 a = LOAD '$datahome' AS (name:chararray, fruit:chararray);
 DUMP a;
 b = FILTER a BY $1 == 'banana';
 c = FILTER a BY $1 == 'banana';
 
 --EXPLAIN c;
 --DUMP c;
 ILLUSTRATE c;
 --STORE b INTO '/tmp/output/bb';
 --STORE c INTO '/tmp/output/cc';