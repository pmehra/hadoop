package com.practice.hadoop;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.KeyValueTextInputFormat;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class InvertCitation extends Configured implements Tool {

	public static class InvertCitationMapper extends MapReduceBase implements
			Mapper<Text, Text, Text, Text> {

		@Override
		public void map(Text key, Text value,
				OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {
			try {
				if (Integer.parseInt(key.toString()) > 0
						&& Integer.parseInt(value.toString()) > 0)
					output.collect(value, key);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

	}

	public static class InvertCitationReducer extends MapReduceBase implements
			Reducer<Text, Text, Text, Text> {

		@Override
		public void reduce(Text key, Iterator<Text> values,
				OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {

			String str = "";
			while (values.hasNext()) {
				str = str + values.next().toString();
				if (values.hasNext()) {
					str += ",";
				}
			}
			reporter.setStatus("key " + key + "  	");
			output.collect(key, new Text(str));
		}

	}

	@Override
	public int run(String[] args) throws Exception {

		if (args.length == 0) {
			String home = "/home/pmehra/data/patent-data";
			args = new String[] { home + "/cite75_99-sample.txt",
					home + "/output1" };
		}
		
		Configuration configuration = new Configuration();
		JobConf jobConf = new JobConf(configuration, InvertCitation.class);

		// input output path
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);

		FileInputFormat.setInputPaths(jobConf, in);
		FileOutputFormat.setOutputPath(jobConf, out);

		// map&reduce classes
		jobConf.setJobName("InvertCitation");
		jobConf.setMapperClass(InvertCitationMapper.class);
		jobConf.setReducerClass(InvertCitationReducer.class);

		// how to read input file
		jobConf.setInputFormat(KeyValueTextInputFormat.class);
		jobConf.set("key.value.separator.in.input.line", ",");
		jobConf.setNumReduceTasks(1);

		// how to output result
		jobConf.setOutputFormat(TextOutputFormat.class);
		jobConf.setOutputKeyClass(Text.class);
		jobConf.setOutputValueClass(Text.class);

		JobClient.runJob(jobConf);
		return 0;
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new InvertCitation(),
				args);
		System.exit(res);
	}

}
