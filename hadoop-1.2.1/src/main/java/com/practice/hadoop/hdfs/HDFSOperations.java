package com.practice.hadoop.hdfs;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.FsUrlStreamHandlerFactory;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.util.Progressable;
import org.apache.log4j.Logger;

public class HDFSOperations {

	public static final String url = "hdfs://localhost:8020/%s";
	private static Logger LOG = Logger.getLogger(HDFSOperations.class);

	public static void main(String[] args) {

	}

	public static void deleteHDFSFile() {

		String path = String.format("hdfs://localhost:8020/%s",
				new Object[] { "data/test.txt" });

		Configuration conf = new Configuration();
		try {
			FileSystem fs = FileSystem.get(URI.create(path), conf);
			boolean delete = fs.delete(new Path(path), true);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean deleteHDFSFile(String location) throws IOException {
		if (location == null || location.trim().isEmpty()) {
			throw new IllegalArgumentException("location is null/empty");
		}
		String path = String.format("hdfs://localhost:8020/%s", location);
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(URI.create(path), conf);
		return fs.delete(new Path(path), true);
	}

	public static boolean existHDFSFile(String location) throws IOException {
		if (location == null || location.trim().isEmpty()) {
			throw new IllegalArgumentException("location is null/empty");
		}
		String path = String.format("hdfs://localhost:8020/%s", location);
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(URI.create(path), conf);
		return fs.exists(new Path(path));
	}

	private static void listHDFSFiles() {
		String path = String.format("hdfs://localhost:8020/%s",
				new Object[] { "data" });
		Configuration conf = new Configuration();
		try {
			FileSystem fs = FileSystem.get(URI.create(path), conf);
			FileStatus[] listStatus = fs.listStatus(new Path(path));
			Path[] paths = FileUtil.stat2Paths(listStatus);
			for (Path filePath : paths) {
				LOG.debug(filePath);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void createFileInHdfs() {
		String localFilePath = "/data/test.txt";
		String hdfsPath = String.format("hdfs://localhost:8020/%s",
				"data/test.txt");

		HDFSOperations hdfsOperations = new HDFSOperations();
		InputStream in = hdfsOperations.getClass().getResourceAsStream(
				localFilePath);
		in = new BufferedInputStream(in);
		Configuration conf = new Configuration();
		FileSystem fs = null;
		try {
			fs = FileSystem.get(URI.create(hdfsPath), conf);
			FSDataOutputStream out = fs.create(new Path(hdfsPath),
					new Progressable() {
						public void progress() {
							HDFSOperations.LOG.debug(".");
						}
					});
			IOUtils.copyBytes(in, out, 4096, true);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeStream(in);
		}
	}

	private static void copyFromLocal(Configuration hdfsconfig, Path inLocal,
			Path outHdfs, String fileNameInHdfs) throws IOException {
		
		if (hdfsconfig == null || inLocal == null || outHdfs == null) {
			throw new IllegalArgumentException(
					"CopyFromLocal input cannot be null");
		}

		LocalFileSystem localFS = FileSystem.getLocal(new Configuration());
		
		if (localFS.exists(inLocal)) {
			throw new IllegalArgumentException("Path does not exist "
					+ inLocal.toString());
		}

		InputStream in = new BufferedInputStream(localFS.open(inLocal));
		FileSystem hdfs = null;
		try {
			hdfs = FileSystem.get(hdfsconfig);
			String outHDFSPath = outHdfs.toString() + "/" + fileNameInHdfs;
			if (hdfs.exists(new Path(outHDFSPath))) {
				throw new IllegalArgumentException(
						"File already present in HDFS " + outHDFSPath);
			}

			FSDataOutputStream out = hdfs.create(new Path(outHDFSPath),
					new Progressable() {
						public void progress() {
							HDFSOperations.LOG.debug(".");
						}
					});
			IOUtils.copyBytes(in, out, 4096, true);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeStream(in);
		}
		
	}

	private static void urlStreamHandlerFactoryReadFS() {
		URL.setURLStreamHandlerFactory(new FsUrlStreamHandlerFactory());
		String data = "data/firstfile.txt";
		InputStream in = null;
		try {
			in = new URL(String.format("hdfs://localhost:8020/%s",
					new Object[] { data })).openStream();
			IOUtils.copyBytes(in, System.out, 4096, false);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeStream(in);
		}
	}

	private static void readFSUsingConfiguration() {
		String path = "data/firstfile.txt";
		Configuration conf = new Configuration();
		FileSystem fs = null;
		InputStream in = null;
		try {
			String fullPath = String.format("hdfs://localhost:8020/%s",
					new Object[] { path });
			fs = FileSystem.get(URI.create(fullPath), conf);
			in = fs.open(new Path(fullPath));
			IOUtils.copyBytes(in, System.out, 4096, false);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeStream(in);
		}
	}

	private static void seekHdfsFileSystem() {
		String path = "data/firstfile.txt";
		Configuration conf = new Configuration();
		FileSystem fs = null;
		InputStream in = null;
		try {
			String fullPath = String.format("hdfs://localhost:8020/%s",
					new Object[] { path });
			fs = FileSystem.get(URI.create(fullPath), conf);
			in = fs.open(new Path(fullPath));
			IOUtils.copyBytes(in, System.out, 4096, false);
			FSDataInputStream fdin = (FSDataInputStream) in;
			fdin.seek(0L);
			IOUtils.copyBytes(fdin, System.out, 4096, false);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeStream(in);
		}
	}
}