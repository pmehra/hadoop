package com.practice.hadoop.hdfs;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class PutMerge {
	
	public static final String url = "hdfs://localhost:8020/%s";

	public static void main(String[] args) throws IOException {

		if (args.length < 2 || (args[0] == null || args[1] == null)) {
			throw new IllegalArgumentException("Incompatible arguments : "
					+ Arrays.toString(args));
		}
		
		String destination = String.format(url, args[1]);
		Configuration conf = new Configuration();
		
		FileSystem local = FileSystem.getLocal(conf);
		FileSystem hdfs = FileSystem.get(URI.create(destination),conf);

		Path inputDir = new Path(args[0]);
		Path hdfsFile = new Path(destination);

		if (!local.exists(inputDir)) {
			throw new IllegalArgumentException("Invalid local path " + args[0]);
		}

		if (hdfs.exists(hdfsFile)) {
			throw new IllegalArgumentException("File alreday exist in HDFS at "
					+ args[1]);
		}

		FileStatus[] files = local.listStatus(inputDir);
		FSDataOutputStream output = hdfs.create(hdfsFile);

		for (FileStatus fileStatus : files) {
			System.out.println("Processing file "
					+ fileStatus.getPath().getName());
			FSDataInputStream fsInput = local.open(fileStatus.getPath());

			byte[] buffer = new byte[256];
			int bytesRead = 0;
			while ((bytesRead = fsInput.read(buffer)) > 0) {
				output.write(buffer, 0, bytesRead);
			}
			fsInput.close();
		}
		output.close();
	}

}