package com.practice.hadoop;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.KeyValueTextInputFormat;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class AverageCitation extends Configured implements Tool {

	public enum ClaimsCounter {
		MISSING, QUOTED
	}

	public static class ACMapper extends MapReduceBase implements
			Mapper<Text, Text, Text, IntWritable> {

		public static IntWritable ONE = new IntWritable(1);

		@Override
		public void map(Text key, Text value,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			String country = value.toString().split(",")[3];
			String numCliams = value.toString().split(",")[8];
			reporter.incrCounter(ClaimsCounter.MISSING, 1);
			if (numCliams.length() == 0) {
				reporter.incrCounter(ClaimsCounter.MISSING, 1);
			} else if (numCliams.startsWith("\"")) {
				reporter.incrCounter(ClaimsCounter.QUOTED, 1);
			} else {
				output.collect(new Text(country), ONE);
			}
		}
	}

	public static class ACReducer extends MapReduceBase implements
			Reducer<Text, IntWritable, Text, IntWritable> {

		@Override
		public void reduce(Text key, Iterator<IntWritable> values,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			int sum = 0;
			while (values.hasNext()) {
				sum++;
				values.next();
			}
			output.collect(key, new IntWritable(sum));
		}

	}

	@Override
	public int run(String[] args) throws Exception {

		Configuration configuration = new Configuration();
		JobConf jobConf = new JobConf(configuration, InvertCitation.class);

		// input output path
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);
		FileInputFormat.setInputPaths(jobConf, in);
		FileOutputFormat.setOutputPath(jobConf, out);

		// map&reduce classes
		jobConf.setJobName("AverageCitation");
		jobConf.setMapperClass(ACMapper.class);
		jobConf.setReducerClass(ACReducer.class);

		// how to read input file
		jobConf.setInputFormat(KeyValueTextInputFormat.class);
		jobConf.set("key.value.separator.in.input.line", ",");

		// how to output result
		jobConf.setOutputFormat(TextOutputFormat.class);
		jobConf.setOutputKeyClass(Text.class);
		jobConf.setOutputValueClass(IntWritable.class);

		JobClient.runJob(jobConf);
		return 0;
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new AverageCitation(),
				args);
		System.exit(res);
	}
}
