package com.practice.hadoop;

import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.KeyValueTextInputFormat;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.google.common.collect.MinMaxPriorityQueue;
import com.practice.hadoop.hdfs.HDFSOperations;

public class ChainMR extends Configured implements Tool {

	public static class InvertMapper extends MapReduceBase implements
			Mapper<Text, Text, Text, IntWritable> {

		public static IntWritable ONE = new IntWritable(1);

		@Override
		public void map(Text key, Text value,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			output.collect(value, ONE);
		}

	}

	public static class InvertReducer extends MapReduceBase implements
			Reducer<Text, IntWritable, Text, IntWritable> {

		@Override
		public void reduce(Text key, Iterator<IntWritable> values,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {

			int count = 0;
			while (values.hasNext()) {
				values.next();
				count++;
			}
			output.collect(key, new IntWritable(count));
		}

	}

	public static class MaxMapper extends MapReduceBase implements
			Mapper<Text, Text, Text, IntWritable> {

		@Override
		public void map(Text key, Text value,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			output.collect(key,
					new IntWritable(Integer.parseInt(value.toString())));
		}

	}

	public static class MaxReduer extends MapReduceBase implements
			Reducer<Text, IntWritable, Text, IntWritable> {

		private MinMaxPriorityQueue<MaxCitation> queue = MinMaxPriorityQueue
				.maximumSize(10).create();
		private OutputCollector<Text, IntWritable> mainOutput = null;

		@Override
		public void reduce(Text key, Iterator<IntWritable> values,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {

			if (mainOutput == null) {
				mainOutput = output;
			}

			int count = 0;
			while (values.hasNext()) {
				count += values.next().get();
			}
			queue.offer(new MaxCitation(key.toString(), count));
		}

		@Override
		public void close() throws IOException {
			super.close();
			for (MaxCitation maxCitation : queue) {
				mainOutput.collect(new Text(maxCitation.getCitation()),
						new IntWritable(maxCitation.getCount().intValue()));
			}
		}

	}

	@Override
	public int run(String[] args) throws Exception {

		Configuration configuration = new Configuration();
		JobConf jobConfOne = new JobConf(configuration, ChainMR.class);

		// input output path
		Path inOne = new Path(args[0]);
		Path outOne = new Path(args[1] + "/output");

		if (HDFSOperations.existHDFSFile(args[1] + "/output")) {
			HDFSOperations.deleteHDFSFile(args[1] + "/output");
		}

		FileInputFormat.setInputPaths(jobConfOne, inOne);
		FileOutputFormat.setOutputPath(jobConfOne, outOne);

		// map&reduce classes
		jobConfOne.setJobName("InvertMR");
		jobConfOne.setMapperClass(InvertMapper.class);
		jobConfOne.setReducerClass(InvertReducer.class);

		// how to read input file
		jobConfOne.setInputFormat(KeyValueTextInputFormat.class);
		jobConfOne.set("key.value.separator.in.input.line", ",");

		// how to output result
		jobConfOne.setOutputFormat(TextOutputFormat.class);
		jobConfOne.setOutputKeyClass(Text.class);
		jobConfOne.setOutputValueClass(IntWritable.class);

		JobClient.runJob(jobConfOne);

		JobConf jobConfTwo = new JobConf(configuration, ChainMR.class);
		Path inTwo = new Path(args[1] + "/output/part-00000");
		Path outTwo = new Path(args[1] + "/output2");

		if (HDFSOperations.existHDFSFile(args[1] + "/output2")) {
			HDFSOperations.deleteHDFSFile(args[1] + "/output2");
		}

		FileInputFormat.setInputPaths(jobConfTwo, inTwo);
		FileOutputFormat.setOutputPath(jobConfTwo, outTwo);

		// map&reduce classes
		jobConfTwo.setJobName("MaxCitationMR");
		jobConfTwo.setMapperClass(MaxMapper.class);
		jobConfTwo.setReducerClass(MaxReduer.class);

		// how to read input file
		jobConfTwo.setInputFormat(KeyValueTextInputFormat.class);

		// how to output result
		jobConfTwo.setOutputFormat(TextOutputFormat.class);
		jobConfTwo.setOutputKeyClass(Text.class);
		jobConfTwo.setOutputValueClass(IntWritable.class);

		JobClient.runJob(jobConfTwo);
		/*
		 * Job jobOne = new Job(jobConfOne); Job jobTwo = new Job(jobConfTwo);
		 * jobTwo.addDependingJob(jobOne);
		 */
		/*
		 * JobControl jobControl = new JobControl("ChainAll");
		 * jobControl.addJob(jobTwo); jobControl.run();
		 */
		return 0;
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new ChainMR(), args);
		System.exit(res);
	}

	private static class MaxCitation implements Serializable,
			Comparable<MaxCitation> {
		private static final long serialVersionUID = 3494219828355683656L;
		private final String citation;
		private final Integer count;

		public MaxCitation(String citation, Integer count) {
			super();
			this.citation = citation;
			this.count = count;
		}

		public String getCitation() {
			return citation;
		}

		public Integer getCount() {
			return count;
		}

		@Override
		public int compareTo(MaxCitation othrVal) {
			return othrVal.getCount().compareTo(count);
		}

	}

}
