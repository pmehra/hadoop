package com.practice.hadoop.join;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.contrib.utils.join.DataJoinMapperBase;
import org.apache.hadoop.contrib.utils.join.DataJoinReducerBase;
import org.apache.hadoop.contrib.utils.join.TaggedMapOutput;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.practice.hadoop.hdfs.HDFSOperations;

/**
 * Reduce side join or partitioned sort-merge join
 * 
 * @author pmehra
 */
public class DataJoin extends Configured implements Tool {

	public static class DataJoinMapper extends DataJoinMapperBase {

		@Override
		protected Text generateInputTag(String inputFile) {
			String datasource = inputFile.split("-")[0];
			return new Text(datasource);
		}

		@Override
		protected TaggedMapOutput generateTaggedMapOutput(Object value) {
			return new TaggedWritable((Text) value, inputTag);
		}

		@Override
		protected Text generateGroupKey(TaggedMapOutput aRecord) {
			String grpKey = aRecord.getData().toString().split(",")[0];
			return new Text(grpKey);
		}

	}

	public static class DataJoinReducer extends DataJoinReducerBase {

		@Override
		protected TaggedMapOutput combine(Object[] tags, Object[] values) {
			if (tags.length < 2) {
				return null;
			}
			String joinedStr = "";
			for (int i = 0; i < values.length; i++) {
				if (i > 0) {
					joinedStr += ",";
				}

				TaggedWritable tw = (TaggedWritable) values[i];
				String[] splits = tw.getData().toString().split(",", 2);
				joinedStr += splits[1];
			}

			return new TaggedWritable(new Text(joinedStr), (Text) tags[0]);

		}
	}

	@Override
	public int run(String[] args) throws Exception {

		Configuration configuration = new Configuration();
		JobConf jobConf = new JobConf(configuration, DataJoin.class);

		// input output path
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);

		if (HDFSOperations.existHDFSFile(args[1] + "/output")) {
			HDFSOperations.deleteHDFSFile(args[1] + "/output");
		}

		FileInputFormat.setInputPaths(jobConf, in);
		FileOutputFormat.setOutputPath(jobConf, out);

		// map&reduce classes
		jobConf.setJobName("DataJoin");
		jobConf.setMapperClass(DataJoinMapper.class);
		jobConf.setReducerClass(DataJoinReducer.class);

		// how to read input file
		jobConf.setInputFormat(TextInputFormat.class);

		// how to output result
		jobConf.setOutputFormat(TextOutputFormat.class);
		jobConf.setOutputKeyClass(Text.class);
		jobConf.setOutputValueClass(TaggedWritable.class);
		jobConf.set("mapred.textoutputformat.separator", ",");

		JobClient.runJob(jobConf);
		return 0;
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new DataJoin(), args);
		System.exit(res);
	}

}
