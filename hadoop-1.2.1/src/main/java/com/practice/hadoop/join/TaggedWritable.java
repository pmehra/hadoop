package com.practice.hadoop.join;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.contrib.utils.join.TaggedMapOutput;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

/**
 * Tagged output class for joining two data sets. This class will be used as
 * value in Map output. Group key will be used as key.
 * 
 * @author pmehra
 */
public class TaggedWritable extends TaggedMapOutput {

	final private Text value;

	public TaggedWritable(Text data, Text tag) {
		this.value = data;
		this.tag = tag;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.tag.readFields(in);
		this.value.readFields(in);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		this.tag.write(out);
		this.value.write(out);
	}

	@Override
	public Writable getData() {
		return value;
	}

}
