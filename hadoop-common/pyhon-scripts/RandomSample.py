#this program will get random sample data from the file.
#User has to give % between 1-100, as how much samle data he needs.
#cat FILE | python ./RandomSample.py PERCENT > OUTPUT-FILE


#!/usr/bin/env python
import sys, random

for line in sys.stdin:
  if(random.randint(1,100) <= int(sys.argv[1])):
		print line.strip()